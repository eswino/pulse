EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "pulseWristBand"
Date "2022-03-26"
Rev "v004"
Comp "IKNX - ESWINO Corps"
Comment1 "tmp_sensor_sheet"
Comment2 "Designer: fmaw"
Comment3 "Reviewer: ace"
Comment4 "Manufacturer : IKNX "
$EndDescr
$Comp
L pulsera_wifi-rescue:MAX30205-_CI U7
U 1 1 60C9C342
P 4825 2700
F 0 "U7" H 4825 3115 50  0000 C CNN
F 1 "MAX30205" H 4825 3024 50  0000 C CNN
F 2 "_Sensores:TDFN-3.1x3.1mm-p0.65mm-8pin-MAX30205" H 4825 3300 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX30205.pdf" H 4575 2850 50  0001 C CNN
	1    4825 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4475 2850 4325 2850
Text GLabel 4475 2550 0    50   Input ~ 0
SDA
Text GLabel 4475 2650 0    50   Input ~ 0
SCL
$Comp
L power:+3.3V #PWR?
U 1 1 60CA260E
P 5550 2300
AR Path="/60CA260E" Ref="#PWR?"  Part="1" 
AR Path="/60BF156C/60CA260E" Ref="#PWR093"  Part="1" 
F 0 "#PWR093" H 5550 2150 50  0001 C CNN
F 1 "+3.3V" H 5565 2473 50  0000 C CNN
F 2 "" H 5550 2300 50  0001 C CNN
F 3 "" H 5550 2300 50  0001 C CNN
	1    5550 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2300 5550 2550
Wire Wire Line
	5550 2550 5175 2550
$Comp
L power:+3.3V #PWR?
U 1 1 60CA2762
P 4125 2275
AR Path="/60CA2762" Ref="#PWR?"  Part="1" 
AR Path="/60BF156C/60CA2762" Ref="#PWR092"  Part="1" 
F 0 "#PWR092" H 4125 2125 50  0001 C CNN
F 1 "+3.3V" H 4140 2448 50  0000 C CNN
F 2 "" H 4125 2275 50  0001 C CNN
F 3 "" H 4125 2275 50  0001 C CNN
	1    4125 2275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5175 2650 5325 2650
Wire Wire Line
	5325 2650 5325 2750
Wire Wire Line
	5175 2750 5325 2750
Connection ~ 5325 2750
Wire Wire Line
	5325 2750 5325 2850
Wire Wire Line
	5175 2850 5325 2850
Connection ~ 5325 2850
Wire Wire Line
	4125 2275 4125 2350
Wire Wire Line
	4125 2750 4475 2750
Wire Wire Line
	4125 2650 4125 2750
Wire Wire Line
	4325 2850 4325 3200
$Comp
L power:GNDD #PWR095
U 1 1 60CB8CD8
P 4825 3400
F 0 "#PWR095" H 4825 3150 50  0001 C CNN
F 1 "GNDD" H 4829 3245 50  0000 C CNN
F 2 "" H 4825 3400 50  0001 C CNN
F 3 "" H 4825 3400 50  0001 C CNN
	1    4825 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60CC35CB
P 5550 2775
AR Path="/60CC35CB" Ref="C?"  Part="1" 
AR Path="/60BF156C/60CC35CB" Ref="C38"  Part="1" 
F 0 "C38" H 5665 2821 50  0000 L CNN
F 1 "100nF" H 5665 2730 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5588 2625 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 5550 2775 50  0001 C CNN
F 4 "0402YD104KAT2A" H 5550 2775 50  0001 C CNN "Ref."
	1    5550 2775
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2550 5550 2625
Connection ~ 5550 2550
$Comp
L power:GNDD #PWR094
U 1 1 60CC3B29
P 5550 3400
F 0 "#PWR094" H 5550 3150 50  0001 C CNN
F 1 "GNDD" H 5554 3245 50  0000 C CNN
F 2 "" H 5550 3400 50  0001 C CNN
F 3 "" H 5550 3400 50  0001 C CNN
	1    5550 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3400 5550 2925
Wire Wire Line
	4825 3050 4825 3200
Wire Wire Line
	5325 2850 5325 3200
$Comp
L Device:R R?
U 1 1 60CCB275
P 4125 2500
AR Path="/60B3AA65/60CCB275" Ref="R?"  Part="1" 
AR Path="/60BF156C/60CCB275" Ref="R19"  Part="1" 
F 0 "R19" V 4025 2550 50  0000 C CNN
F 1 "4,7k" V 4125 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4055 2500 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 4125 2500 50  0001 C CNN
F 4 "Yageo" H 4125 2500 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-074K7L" H 4125 2500 50  0001 C CNN "Ref."
	1    4125 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4325 3200 4825 3200
Connection ~ 4825 3200
Wire Wire Line
	4825 3200 5325 3200
Wire Wire Line
	4825 3200 4825 3400
$EndSCHEMATC
