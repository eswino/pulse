EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "pulseWristBand"
Date "2022-03-26"
Rev "v004"
Comp "IKNX - ESWINO Corps"
Comment1 "flash_memory_sheet"
Comment2 "Designer: fmaw"
Comment3 "Reviewer: ace"
Comment4 "Manufacturer : IKNX "
$EndDescr
$Comp
L Memory_Flash:MX25R3235FM1xx0 U9
U 1 1 61BBAE6C
P 4400 2100
F 0 "U9" H 4850 2500 50  0000 L CNN
F 1 "MX25R3235FM1xx0" H 4850 2400 50  0000 L CNN
F 2 "_genericSMDdevices:wson8_8.0x6.0mm_pitch1.27mm_emulator" H 4400 1500 50  0001 C CNN
F 3 "http://www.macronix.com/Lists/Datasheet/Attachments/7534/MX25R3235F,%20Wide%20Range,%2032Mb,%20v1.6.pdf" H 4400 2100 50  0001 C CNN
	1    4400 2100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 61BC4607
P 4600 1700
AR Path="/60B3AA65/61BC4607" Ref="#PWR?"  Part="1" 
AR Path="/6199965B/61BC4607" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 4600 1550 50  0001 C CNN
F 1 "+3.3V" V 4600 1950 50  0000 C CNN
F 2 "" H 4600 1700 50  0001 C CNN
F 3 "" H 4600 1700 50  0001 C CNN
	1    4600 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 61BC460D
P 4600 2500
AR Path="/60B3AA65/61BC460D" Ref="#PWR?"  Part="1" 
AR Path="/6199965B/61BC460D" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 4600 2250 50  0001 C CNN
F 1 "GNDD" H 4604 2345 50  0000 C CNN
F 2 "" H 4600 2500 50  0001 C CNN
F 3 "" H 4600 2500 50  0001 C CNN
	1    4600 2500
	1    0    0    -1  
$EndComp
Text GLabel 3900 2000 0    50   Input ~ 0
SPICLK
Text GLabel 4900 2100 2    50   Input ~ 0
SPICS0
Text GLabel 3900 2200 0    50   Input ~ 0
SPIWP
Text GLabel 3900 2100 0    50   Input ~ 0
SPIHD
Text GLabel 3900 1900 0    50   Input ~ 0
SPICS1
NoConn ~ 3900 2300
$EndSCHEMATC
