#include "libs/timers32.h"
#include <WiFi.h>
#include <PubSubClient.h>
#include "temperature_sensor.h"

// Replace the next variables with your SSID/Password combination
const char* ssid = "daWifi";
const char* password = "F+E=Niev3s";

// Add your MQTT Broker IP address, example:
//const char* mqtt_server = "192.168.1.144";
const char* mqtt_server = "iknx.dyndns.org";

WiFiClient espClient;
PubSubClient client(espClient);
tempSens TMPSENSOR;
long lastMsg = 0;
char msg[50];
int value = 0;

void setup()
{
  Wire.begin();
  Serial.begin(115200);
  configurarTimer(1, 1, 1, 1);
  arrancarTimer(1, 1, 1, 1);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  TMPSENSOR.init();

}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.print("\r\nConnecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* message, unsigned int length)
{
  Serial.print("Message arrived\r\n");
}

void reconnect() {
  Serial.print("Attempting MQTT connection...");
  // Attempt to connect
  if (client.connect("Mosquitto")) {
    Serial.println("connected");
    // Subscribe
    client.subscribe("sensores/test");
    client.publish("sensores/test", "envío de prueba");
    client.subscribe("sensores/temp");
    client.publish("sensores/temp", "36.7");
    client.subscribe("sensores/presL");
    client.publish("sensores/presL", "07.92");
    client.subscribe("sensores/presH");
    client.publish("sensores/presH", "11.43");
    client.subscribe("sensores/oxg");
    client.publish("sensores/oxg", "95%");
    client.subscribe("sensores/total");
    client.publish("sensores/total", "totalmente de acuerdo en todo!");
    //      client.unsubscribe("sensores/total");

    //      client.subscribe("esp32/join");
  } else {
    Serial.print("failed, rc=");
    Serial.print(client.state());
    Serial.println(" try again later");
    // Wait 5 seconds before retrying
  }
}

void loop()
{
  if (cuenta0 == 0)
  {

    Serial.print("Esto cambia de valor de cuenta0: " + String(cuenta0) + "\r\n");
    (cuenta0 = 1);
    reconnect();
    
    Serial.println(String(TMPSENSOR.charSens()));


  }



}
