/**
  @file wifi32.h
  @brief funciones principales para la gestion del wifi
	Conexiones, Desconexiones, Subida de datos, Consultar IP...
	@author Antonio C�novas
  @date 11/02/2019

*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										WIFI                                                                                           //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										CONJUNTO DE LIBRERIAS                                                                          //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <WiFiAP.h>
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																									  DEFINE Y VARIABLES GLOBALES                                                                             //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//USUARIO Y CONTRASE�A PARA TOMAR LOS DATOS DEL WIFI
const char *ssidTemporal = "12345678";
const char *passwordTemporal = "12345678";

/*//USUARIO Y CONTRASE�A PARA CONECTAR AL WIFI
const char *ssid = "TP-LINK_4A543E";
const char *password = "39773173";
*/
char *ssid;
char *password;

//USUARIO Y CONTRASE�A PARA PUNTO DE ACCESO
const char *ssidAP = "yourAP";
const char *passwordAP = "yourPassword";
//HOST PARA CONOCER IP P�BLICA
const  char * hostPublico = "www.vermiip.es" ;
//VARIABLES PARA LA URL DE SUBIDA DE DATOS
const char* host = "covapps.es";
String url_cabezera = "/ESP32ITH.php";
String url_vaqueriza = "?ID_VAQUERIZA="; 
String url_zona = "&ID_ZONA=";
String url_temperatura = "&Temperatura=";
String url_humedad = "&Humedad=";
String url_ith = "&ITH=";
String url_R0 = "&R0=";
String url_R1 = "&R1=";
String url_R2 = "&R2=";
String url_R3 = "&R3=";
String url_levelBateria= "&Bateria=";
String IP_PublicaN = "";
String IP_PublicaV = "";
WiFiClient client;
WebServer server(80);
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										PROTOTIPADO DE FUNCIONES                                                                       //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
void conectarWifi(void);
void conectarTemporal(void);
void desconectarWifi(void);
void hacerConexion(void);
void envioDatosURL(void);
void consultaIP(void);
void subirIP(void);
void nuevoAP(const char*, const char*);
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										FUNCIONES                                                                                      //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Esta funcion crea un punto de acceso para conectarnos a el.
@param *name, nombre del punto de acceso
@param *pswrd, nombre de la contrase�a para el punto de acceso
@note Esta funcion crea un punto de acceso, por si en cualquier momento fuera necesario usarla
NO ESTA USADA EN EL FLUJO DEL PROGRAMA*/
void nuevoAP(const char* name, const char* pswrd){
	WiFi.softAP(name, pswrd);
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.begin();
}
/**
Esta funcion se conecta a un punto de acceso con el ssid y la clave 12345678
@note solo sera necesario para una configuracion inicial, de aqu� coge los datos del ssid y password original y los almacena en la EEPROM
*/
void conectarTemporal(void){
  WiFi.mode(WIFI_STA);
	WiFi.begin(ssidTemporal, passwordTemporal);
  while (WiFi.status() != WL_CONNECTED) {
     delay(500);   
		Serial.print(".");
    }
	if(DBUG == 1){
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
	}
}

/**
Esta funcion se conecta a un punto de acceso con el ssid y la clave que ha recogido de la base de datos anteriormente.
@note Esta ser� la conexi�n final del dispositivo.*/ 
void conectarWifi(void){
  WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
     delay(500);   
		Serial.print(".");
    }
	if(DBUG == 1){
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
	}
}
/**
Esta funcion desconecta el WiFi
@note esta funcion se usara para desconectar el wifi temporal y despu�s poder conectar el wifi final de la vaqueria*/
void desconectarWifi(){
	WiFi.disconnect();
	WiFi.mode(WIFI_OFF);
	if(DBUG == 1){
  Serial.println("");
  Serial.println("WiFi desconectado");
	}
}
/**
Esta funcion establece la conexion con la base de datos para la posterior subida o recogida de datos.
@param sitio variable bool que nos indicar� si hacemos conexion con covapps (1) o con la pag web que nos devuelve la IP (0).
@note Esta funcion admite un nivel alto o bajo de entrada para seleccionar la url a la que haremos la conexion*/
//ESTABLECE CONEXION CON LA BASE DE DATOS
void hacerConexion(bool sitio){
 const int httpPort = 80;
 const char* conec = "";
 if (sitio == 1){
 conec = host;
 }
 else{
 conec = hostPublico;
 }
 if (!client.connect(conec, httpPort)) {
 Serial.println("connection failed");
 return;
 }
}
/**
Esta funcion se encarga de subir los datos a la base de datos.
@param PaqueteURL contiene la direccion URL que hay que escribir para almacenar los datos en la base de datos
@note Esta funcion necesita previamente preparar el paquete de datos, que lo prepara la funcion paqueteURL (funciones32.h)*/
void envioDatosURL(String PaqueteURL){
	if(DBUG == 1){
			Serial.println(PaqueteURL);
		}
	client.print(String("GET ") + PaqueteURL + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n");
	unsigned long timeout = millis();
	while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      if (DBUG == 1){
				Serial.println(">>> Client Timeout !");
				}
			client.stop();
      return;
    }
  }
  // Read all the lines of the reply from server and print them to Serial
  while (client.available()) {
    String line = client.readStringUntil('\r');
    if (DBUG==1){
			Serial.print(line);
		}
  }
	if (DBUG == 1){
		Serial.println();
		Serial.println("closing connection");
	}
}
/**
Esta funcion consulta la IP P�blica que tiene el dispositivo.
@note Hace una conexion a ver mi ip, y despu�s extrae la IP p�blica que tiene el dispositivo*/
void consultaIP(){
	if (WiFi.status() != WL_CONNECTED){
	conectarWifi();
	}
	hacerConexion(false);
	client.print(String("GET /") + " HTTP/1.1\r\n" + "Host: " + hostPublico + "\r\n" + "Connection: close\r\n" + "\r\n");
	unsigned long timeout = millis();
	while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      if (DBUG == 1){
				Serial.println(">>> Client Timeout !");
				}
			client.stop();
      return;
    }
  }
  // Read all the lines of the reply from server and print them to Serial
  while (client.available()) {
    String line = client.readStringUntil('\n');
		if (line.substring(line.indexOf('T'),line.indexOf('T')+24) == "Tu IP p&uacute;blica es:") {
			IP_PublicaN = line.substring(line.indexOf(':')+2,line.length()-5);
			if (DBUG == 1){
			Serial.println("mi IP publica es: " + IP_PublicaN);
			}
		}
  }
	client.stop();
	if (DBUG == 1){
		Serial.println();
		Serial.println("closing connection ConsultaIP");
	}
}
/**
Esta funcion se encarga de subir la IP p�blica que tiene el dispositivo a la base de datos.
@note Siempre que haya cambiado nuestra IP, hacemos una conexion a covapps y subimos nuestra IP*/
// SUBIR IP P�BLICA SOLO S� IP_P�BLICA != IP_P�BLICA ANTIGUA
void subirIP(void){
	if (IP_PublicaN != IP_PublicaV){
		hacerConexion(1);
		String auxPaquete = "/IP_Publica.php?IP_PUBLICA=\"" + IP_PublicaN + "\"&ID_VAQUERIZA=" + T_ID_VAQUERIZA + "&ID_ZONA=" + T_ID_ZONA;
		if(DBUG == 1){
			Serial.println(auxPaquete);
		}
		client.print(String("GET ") + auxPaquete + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n");
		unsigned long timeout = millis();
		while (client.available() == 0) {
			if (millis() - timeout > 5000) {
				if (DBUG == 1){
					Serial.println(">>> Client Timeout !");
					}
				client.stop();
				return;
			}
		}
		// Read all the lines of the reply from server and print them to Serial
		while (client.available()) {
			String line = client.readStringUntil('\r');
			if (DBUG==1){
				Serial.print(line);
			}
		}
		if (DBUG == 1){
			Serial.println();
			Serial.println("closing connection Subida IP");
		}
		IP_PublicaV = IP_PublicaN;
		}
		else{
		return;
		}
}
/**
Esta funcion engloba a las dos anteriores con el propsito de modular el c�digo.
@note Consultamos nuestra IP p�blica, si esta var�a la a�adimos a nuestra base de datos*/
void obtenerIP(){
	consultaIP();
	subirIP();
}

