/**
  @file LoRa32.h
  @brief funciones principales para gestionar las comunicaciones mediante LoRa
	Inicializacion, Interrupcion, Leer, marcador, envio, escuchar...
	@author Antonio C�novas
  @date 07/02/2019

*/


/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										COMUNICACION LORA                                                                 //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										CONJUNTO DE LIBRERIAS                                                                          //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
#include <LoRa.h>    //COMUNICACION LORA
#include <SPI.h>
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																									DEFINE Y VARIABLES GLOBALES                                                                             //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
#define DBUG 1
//DEFINE LOS PINES Y BANDA CORRESPONDIENTES A LORA SEMTECH SX1276
#define SS      18
#define RST     14
#define DI0     26
#define BAND    868E6

// VARIABLES DE ID_ZONA, ID_VAQUER�A, TEMPERATURA, HUMEDAD Y ITH CORRESPONDIENTE AL DHT22 PARA EL ENVIO
String T_ID_ZONA = "-";
String T_ID_VAQUERIZA = "-";

// VARIABLES DE ID_ZONA, ID_VAQUER�A, TEMPERATURA, HUMEDAD Y ITH CORRESPONDIENTE AL DHT22 PARA LA RECEPCION
String R_temp = "";
String R_hum = "";
String R_ITH = "";
String R_ID_ZONA = "";
String R_ID_VAQUERIZA = "";

// VARIABLES DE ESTADO DE LOS RELES PARA RECEPCION
String R_StatusR0 = "";
String R_StatusR1 = "";
String R_StatusR2 = "";
String R_StatusR3 = "";

//CONTENIDO PAQUETE DE ENTRADA Y SALIDA
String paqueteSalida = "";  //Cadena que envia los datos preparados
String paqueteEntrada = ""; //Cadena que recoge los datos recibidos
String R_marcador = ""; //Prepara el identificador del paquete
String T_marcador = "";
String T_ACK = "OK";
String R_ACK = "";



// VARIABLES PARA LA INTERRUPCION LORA
volatile bool interrupcionLoRa = false; // Flag set by callback to perform read process in main loop
volatile int incomingPacketSize;


/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										PROTOTIPADO DE FUNCIONES                                                                                         //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
void iniciarLoRa(void);
void tipoPaquete(int);
void enviaPaquete (String);
int Recepcion (void);
void onReceive(int);
void readMessage(void);
void escucharLoRa(void);
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										FUNCIONES 										                                                                //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
			
				/* --- INICIO --- */
/**
RUTINA DE SERVICIO A LA INTERRUPCI�N DE LoRa
@note Cada vez que se reciba un paquete, se ejecutar� esta funcion que leer� el paquete recibido y activar� la bandera de interrupci�n.
*/
void onReceive(int packetSize) {
	if (DBUG == 1){
		Serial.println("Paquete Detectado");
	}
	interrupcionLoRa = true;
  incomingPacketSize = packetSize;
	readMessage();
}
/**
Funcion que se encarga de leer el mensaje recibido.
@note En la variable paqueteEntrada (variable global) guardamos el valor*/
void readMessage() {
  // received a packet
	if (DBUG == 1){
		Serial.println("Paquete Detectado: '");
	}
  // read packet
  for (int i = 0; i < incomingPacketSize; i++) {
    paqueteEntrada += (char)LoRa.read();
  }
	Serial.println(paqueteEntrada);
  // print RSSI of packet
  Serial.print("' with RSSI ");
  Serial.println(LoRa.packetRssi());
}
/**
Funcion que inicializa la comunicaci�n LoRa.
@note Cuando terminamos al configuracion, le decimos cual es la funcion que tiene que llamar la interrupcion, y lo ponemos en modo escucha*/
void iniciarLoRa(){
	SPI.begin(5,19,27,18);
	LoRa.setPins(SS,RST,DI0);
  if (!LoRa.begin(BAND)) {
    if(DBUG == 1){
			Serial.println("Fallo al empezar la App LoRa Sender");
			Serial.println("La devolucion del fallo vale: ");
			Serial.println(LoRa.begin(BAND));
		}
		while (1){
			if(DBUG == 1){
				Serial.println("LoRa no conecta, reiniciar");
			}
			ESP.restart();
			}
		}
	LoRa.onReceive(onReceive);
	LoRa.receive();
}

																							/* --- TIPO DE PAQUETE --- */
/**
Funcion que establece el tipo de paquete que vamos a enviar
@param tipo variable que se encarga de actualizar la variable global T_marcador que ser� el indicador del tipo de paquete
@note T_marcador indica cual es el tipo de paquete, para cada tipo de envio mediante LoRa*/
void tipoPaquete (int tipo){
	switch (tipo){
		case 0:
			T_marcador = "0000";			//HEALTH BIT
			break;						
		case 1:
			T_marcador = "0001";			//PAQUETE DE DATOS
			break;
		case 2:
			T_marcador = "0010";			//AUTOM�TICO-MANUAL RELE
			break;
		case 3:
			T_marcador = "0011";			//CONFIGURACION
			break;
		case 4:
			T_marcador = "0100";			// ACK, DAME DATOS
			break;
		case 5:
			T_marcador = "0101";			//Try again
			break;
		default:
			break;
	}
}
																										/* --- ENVIO --- */
/**
Funcion que se encarga de enviar el paquete mediante LoRa
@param Paquete variable de tipo String que va a ser enviada mediante la comunicacion LoRa
@note Al terminar el envio del paquete, volvemos a decirle a donde se dirige la interrupcion y lo ponemos en modo recepcion*/
void enviaPaquete (String Paquete){
 	if(DBUG == 1){
		Serial.println("Preparando la salida del paquete...");
	}
	LoRa.beginPacket();    //Resetea direcci�n FIFO y tama�o payload
  delay(50);
  LoRa.print(Paquete);	  
  delay(50);
  LoRa.endPacket();			//Pone en modo transmisi�n, espera a que se transmita y limpia las banderas
	LoRa.onReceive(onReceive);
	LoRa.receive();
	if(DBUG == 1){
		Serial.println("Paquete enviado");
	}
}
/**
Funcion que recoge a otras funciones
@note NO USADA EN FLUJO DE PROGRAMA*/
void escucharLoRa(){
	  // register the receive callback
  // put the radio into receive mode
  LoRa.onReceive(onReceive);
	LoRa.receive();
	interrupcionLoRa = true;
}



