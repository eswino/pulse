/**
  @file funciones32.h
  @brief Funciones que engloban las funciones creadas en los demas archivos.
	Encontramos funciones generales como inicializar el dipositivo, descomponer los paquetes de entrada, 
	las funciones que actuan cuando la bandera de la interrupcion esta a 1, subida de datos a la base de datos,
	la preparacion de los paquetes...
  @author Antonio Cánovas
  @date 07/02/2019
*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										PLACA DE DESARROLLO LORA WIFI ESP32 V2                                                         //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																								¿ QUE QUEDA POR HACER ?                                                                            //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										CONJUNTO DE LIBRERIAS                                                                          //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
#include <Arduino.h>
#include <oled32.h> 
#include <LoRa32.h>
#include <sensorDHT2232.h>
#include <timers32.h>
#include <rele32.h>
#include <wifi32.h>
#include <chip32.h>
#include <EEPROM32.h>
#include <ledSwitch32.h>
#include <bateria32.h>
#include <Separador.h>    //LIBRERIA PARA DESCOMPONER EL PAQUETE

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																									DEFINE Y VARIABLES GLOBALES                                                                             //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

//MODO INFORMACIÓN POR PUERTO SERIE. SI QUIERES MODO DEPURACIÓN DBUG = 1
#define DBUG 1

int tipoDeFuncion;											// 0 --- CONCENTRADOR															
																			// 1 --- NODO CON BATERIA
																			// 2 --- NODO SIN BATERIA

Separador s;

//VARIABLE PARA INTERPRETAR SI HAY DATO O NO EN UNA FUNCIÓN
int dato;

//VARIABLE BOOLEANA QUE INTERPRETA SI ES AUTOMATICO O MANUAL PARA ACTIVAR LOS RELES --- automaático 1 y manual 0
bool T_auto_manual = 1;
String R_auto_manual = "";

//VARIABLE PARA IDENTIFICAR SI SE HA RECIBIDO ALGO POR UN NODO
bool recibidoNodo = 0; 					//1 indica que se ha recibido de un nodo

//VARIABLE QUE SE UTILIZARÁ PARA VER SI EL PAQUETE TIENE O NO INTEGRIDAD
bool comprobar = 0;

//VARIABLE QUE IDENTIFICA SI HA HABIDO O NO HEALTH BIT
bool healthBit = 0;

//VARIABLE QUE IDENTIFICA SI SE HA RECIBIDO O NO CONTESTACION AL HEALTBIT
bool esperaPaquete = 0;

//VARIABLE AUXILIAR PARA CONFIGURAR VAQUERIZA Y ZONA
String R_auxchipid = "";

//VARIABLE AUXILIAR PARA VERIFICAR NODO CON BATERÍA PUEDE DORMIR
bool tareaCumplida = 0;
bool empezarTarea = 1;

//VARIABLE AUXILIAR PARA REPETIR PAQUETE EN CASO DE ERROR
String R_tryAgain = "";
//VARIALBE AUXILIAR PARA RECOGER ID VAQUERIZA E ID ZONA DE LA CONFIGURACION
String auxConfiguracionVaquerizaZona = "";
//VARIALBE AUXILIAR PARA VERIFICAR EL ESTADO DEL DISPOSTIVO (AUTO O MANUAL)
String auxConfiguracionAutoManual = "";

//VARIABLES PARA GARANTIZAR EL ENVIO DEL PAQUETE DE CONFIGURACION DE LOS RELES
String auto_manualV = "";
String R_StatusR0V = "";
String R_StatusR1V = "";
String R_StatusR2V = "";
String R_StatusR3V = "";

String ssidB;
String passwordB;
char buf1 [25];
unsigned int leng1 = 50;
char buf2 [25];
unsigned int leng2 = 50;

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										PROTOTIPADO DE FUNCIONES                                                                                         //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

//FUNCIONES GLOBALES
void inicializar(void);
void siHayDatoDHT(void);
void comprobarITH (int);
void InterrupcionTimer0(void);
void InterrupcionTimer1(void);
void InterrupcionTimer2(void);
void InterrupcionTimer3(void);
void compruebaInterrupcion(void);
String paqueteURL (bool);
void preparaPaqueteDatos (uint32_t, String, String, String, float, float, int, bool, bool, bool, bool);
void preparaPaqueteRele (uint32_t, String, String, String, bool, bool, bool, bool);
void descomponerPaquete (String);
bool calcularOR(void);
bool verificaReles(void);
void configuraVaqueriaZona(void);
void Adormir(void);
void paqueteLoRa(void);
void descomponerConfiguracionBBDD(bool, String);
void actualizarDatos(bool);
void descomponerConfiguracionAutoManual (String);
void comprobarAutoManual(void);
bool comprobarCambiosRele(String);
void actualizarEstadosRele(void);
void actualizarAutoManualRele(void);
void enviaHealthBit(void);
void descomponerConfiguracionTemporal(String);
void iniciarWifiTemporal(void);
void enviarDatosEEPROM(void);
bool consultarDatosEEPROM(void);
void inicializacionBasica(void);
void queFuncionHago(void);
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										FUNCIONES QUE ENGLOBAN A OTRAS                                                                 //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
void queFuncionHago(){
	if (concentrador == 0){
		if (bateria == 0){
			tipoDeFuncion = 2; 	//NODO SIN BATERÍA
		}
		else{
			tipoDeFuncion = 1;  //NODO CON BATERÍA
		}
	}
	else{
		tipoDeFuncion = 0;		//CONCENTRADOR
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										INICIALIZAR EL DISPOSITIVO                                                               //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion principal de inicializacion 
@note Engloba las inicializaciones de las otras funciones, LoRa, DHT, CHIP, OLED...
Distingue si el dispositivo tiene bateria o no, en caso de no tener batería, configurará los reles, timers
y si es concentrador, EEPROM, Consulta si tiene los datos ssid y password y si no creará el punto de acceso
*/
void inicializar(){
  Serial.begin(115200);
  iniciarLoRa();
  dhtSensor.begin();
	chipID();
	analizaSwitch();
	configuraLEDs();
	hayBateria();
	Serial.println(bateria);
	if (bateria == 0){
		T_levelBateria = 99.99;
		des_activaLEDs(true, false);
		configurarRele();
		configurarTimer(false,false,false,true);
		arrancarTimer(false,false,false,true);	
		if (concentrador == 1){
			Serial.println("Estoy con wifi liao");
			inicializarEEPROM();
			inicializacionBasica();
			actualizarDatos(0);
		}
	des_activaLEDs(false, true);
	}
	else{
		configuracionBateria();
	}
	iniciarOLED();
	errorOLED("La configuracion está OK");	
	queFuncionHago();
	if(DBUG == 1){
		Serial.println("Velocidad puerto serie 115200 bps, SPI inicializado, pantalla preparada, LoRa Ok, DHT preparado, Switch OK, Relés OK, timer concentrador OK");
		Serial.println("Mi ID_Vaqueria es: " + T_ID_VAQUERIZA + "\nMi ID_Zona es: " + T_ID_ZONA);
		Serial.println("Termine configuración");
	}	
}
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										GESTION DE LOS TIMER POR BANDERA                                                               //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que se ejecutará tras activarse la bandera de interrupcion timer3
@note Cada 20 minutos se ejecutará esta interrupcion que consiste en
leer el sensor DHT22 y en caso de ser el concentrador consultar la IP por si ha cambiado.
*/
void InterrupcionTimer3 (){
	if (contadorInterrupcionConcentrador == 1){
    portENTER_CRITICAL(&timer3Mux);
    contadorInterrupcionConcentrador = 0;
    portEXIT_CRITICAL(&timer3Mux);
		if (concentrador == 1){
			obtenerIP();
		}
		leerDHT22();
		if (concentrador == 0){
			healthBit = 0;
		}
    if (DBUG == 1){
		Serial.println(contadorInterrupcionConcentrador);
		contadorTotalInterrupcionConcentrador++;
    Serial.println("se han producido un total de: " + String(contadorTotalInterrupcionConcentrador) + " interrupciones por timer 3 (Concentrador)");   
			}  
    }
}
/**
Funcion que se ejecutará tras activarse la bandera de interrupcion timer2
@note En caso de que ITH haya sido un valor superior al indice establecido, habrá saltado en rele
y el timer de 5 minutos que lo controla se habrá inicializado. Tras el paso de este, salta la interrupcion y lo detiene
*/
void InterrupcionTimer2 (){
	if (contadorInterrupcion5 == 1){
    portENTER_CRITICAL(&timer2Mux);
    contadorInterrupcion5 = 0;
    portEXIT_CRITICAL(&timer2Mux);
		des_activaRele0(false);
    if (DBUG == 1){
		contadorTotalInterrupcion5++;
    Serial.println(contadorInterrupcion5);
		Serial.println("se han producido un total de: " + String(contadorTotalInterrupcion5) + " interrupciones por timer 0");   
			}
		}
}
/**
Funcion que se ejecutará tras activarse la bandera de interrupcion timer1
@note En caso de que ITH haya sido un valor superior al indice establecido, habrá saltado en rele
y el timer de 15 minutos que lo controla se habrá inicializado. Tras el paso de este, salta la interrupcion y lo detiene
*/
void InterrupcionTimer1 (){
	if (contadorInterrupcion15 == 1){
    portENTER_CRITICAL(&timer1Mux);
    contadorInterrupcion15 = 0;
    portEXIT_CRITICAL(&timer1Mux);
		des_activaRele1(false);
    if (DBUG == 1){
		Serial.println(contadorInterrupcion15);
		contadorTotalInterrupcion15++;
    Serial.println("se han producido un total de: " + String(contadorTotalInterrupcion15) + " interrupciones por timer 1");   
			}
		}
}
/**
Funcion que se ejecutará tras activarse la bandera de interrupcion timer0
@note En caso de que ITH haya sido un valor superior al indice establecido, habrá saltado en rele
y el timer de 20 minutos que lo controla se habrá inicializado. Tras el paso de este, salta la interrupcion y lo detiene
*/
void InterrupcionTimer0 (){
	if (contadorInterrupcion20 == 1){
    portENTER_CRITICAL(&timer0Mux);
    contadorInterrupcion20 = 0;
    portEXIT_CRITICAL(&timer0Mux);
		des_activaRele2(false);
		des_activaRele3(false);
		if (DBUG == 1){
		Serial.println(contadorInterrupcion20);
		contadorTotalInterrupcion20++;
    Serial.println("se han producido un total de: " + String(contadorTotalInterrupcion5) + " interrupciones por timer 2");   
			}
		}
}
/**
Funcion que permite la lecutra de todas las interrupciones por si se han producido
*/
void compruebaInterrupcion(){
	InterrupcionTimer3();
	InterrupcionTimer2();
	InterrupcionTimer1();
	InterrupcionTimer0();
}
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										CALCULAR ITH                                                                //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que en caso de que se haya leido el sensor DHT22, si es el concentrador subirá los datos a la BBDD
@note Lineas comentadas, anteriormente se enviaban los datos después de leerlos, ahora se envian después del HEALTH BIT, no después de leerlos*/
void siHayDatoDHT(){
	if (flagDHT22 == 1){
		T_ITH = calcularITH(T_temp, T_hum);
		comprobarITH(T_ITH);
		errorOLED("ITH CALCULADO");
		des_activaLEDs(false, false);
		if (concentrador == 1){	
			String PaqueteURL = paqueteURL(recibidoNodo);
			hacerConexion(true);										// ESTE SUBE LOS DATOS A LA BASE DE DATOS
			envioDatosURL(PaqueteURL);
		}
		else{
/*			tipoPaquete(1);
			preparaPaqueteDatos(chipid, T_marcador, T_ID_VAQUERIZA, T_ID_ZONA, T_temp, T_hum, T_ITH, T_StatusR0, T_StatusR1, T_StatusR2, T_StatusR3);
			enviaPaquete(paqueteSalida); */
		}
		flagDHT22 = 0;
	}
}
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										COMPROBAR ITH PARA ACTIVAR RELE                                                               //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que comprueba que el ITH no sea superior al umbral establecido
@param ITH el ITH calculado se comprueba con el indice establecido
@note En caso de que ITH haya sido un valor superior al indice establecido, y nos encontremos en modo automatico
saltarán los reles y configuramos y arrancamos los TIMERS que gestionan los reles (No el controla el flujo de programa) 
*/
void comprobarITH (int ITH){							
	if(ITH >= indiceITH){
		if(T_auto_manual == 1 && bateria == 0){   // si no tiene batería el dispositivo y esta en modo automático
			des_activaReles(HIGH);								// activa los relés (todos)
			configurarTimer(true,true,true,false);// configura los timers que se van a usar (0,1,2)
			arrancarTimer(true,true,true,false);  // y arranca los timers que se han configurado
		}
		else{
			return;
		}
	}
}
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										CREAR PAQUETES DE DATOS                                                                        //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que crea un paquete de datos
@param recibido esta variable bool indica si el paquete se forma con los datos propios del concentrador (0)
o se forman con los datos recibidos del nodo (1)
@returns String que es el paquete de datos que se sube a la base de datos
@note creara un paquete de datos en funcion de si es el propio concentrador quien
lo solicita o bien si es un nodo quien ha mandado los datos. Esta preparacion
es para poder subir los datos a la base de datos. Las variables de recepcion se identifican por R_ y las de 
transmición por T_
*/
String paqueteURL (bool recibido){
String Paquete = "";
	if (recibido == 0){
		Paquete = url_cabezera + url_vaqueriza + T_ID_VAQUERIZA + url_zona + T_ID_ZONA + url_temperatura + String(T_temp) + url_humedad + String(T_hum) + url_ith + String(T_ITH) + url_R0 + String(T_StatusR0) + url_R1 + String(T_StatusR1) + url_R2 + String(T_StatusR2) + url_R3 + String(T_StatusR3) + url_levelBateria + String(T_levelBateria);
		recibidoNodo = 0;
		return Paquete;
	}
	else{
		Paquete = url_cabezera + url_vaqueriza + R_ID_VAQUERIZA + url_zona + R_ID_ZONA + url_temperatura + R_temp + url_humedad + R_hum + url_ith + R_ITH + url_R0 + R_StatusR0 + url_R1 + R_StatusR1 + url_R2 + R_StatusR2 + url_R3 + R_StatusR3 + url_levelBateria + R_levelBateria;
		recibidoNodo = 0;
		return Paquete;
	}
}
/**
Funcion prepara un paquete de datos para envio LoRa
@param chip_id el valor del serial number (32bits) del dispositivo que envia
@param marcador tipo de paquete que va a ser 
@param ID_Vaqueriza	el ID de la vaqueriza en la que se encuentra
@param ID_ZONA	el ID de la zona en la que se encuentra
@param temperatura el valor de la temperatura recogida por el sensor
@param humedad el valor de la humedad recogido por el sensor
@param ITH el valor del ITH calculado 
@param Relex el estado en el que se encuentra el RELE 0
@param Reley el estado en el que se encuentra el RELE 1
@param Relez el estado en el que se encuentra el RELE 2
@param Relet el estado en el que se encuentra el RELE 3
@note actualizamos la variable global paqueteSalida con el contenido de la cadena a mandar.
T_EIC es una variable igual que chip_id para darle integridad al paquete, al descomponerlo que coincida.
*/
void preparaPaqueteDatos (uint32_t chip_id, String marcador, String ID_Vaqueriza, String ID_ZONA, float temperatura, float humedad, int ITH, bool Relex, bool Reley, bool Relez, bool Relet, float bateria){
  T_EIC = chip_id; 
  paqueteSalida = String(chip_id, HEX) + " " + marcador + " " + ID_Vaqueriza + " " + ID_ZONA + " " + String(temperatura) + " " + String(humedad) + " " + String(ITH) + " " + String(Relex) + " " + String(Reley) + " " + String(Relez) + " " + String(Relet) + " " + String(bateria) + " " + String(T_EIC, HEX) + " ";
  if(DBUG == 1){
    Serial.println("El paquete de datos preparado para su salida es: " + paqueteSalida);
  }
}
/**
Funcion prepara un paquete de el estado de los reles para envio LoRa
@param chip_id el valor del serial number (32bits) del dispositivo que envia
@param marcador tipo de paquete que va a ser 
@param ID_Vaqueriza	el ID de la vaqueriza a la que va el paquete
@param ID_ZONA	el ID de la zona a la que va el paquete
@param a_m el valor que indica si se encuentra en modo automatico (1) o modo manual (0)   
@param Relex el valor al que tiene que ponerse el RELE 0
@param Reley el valor al que tiene que ponerse el RELE 1
@param Relez el valor al que tiene que ponerse el RELE 2
@param Relet el valor al que tiene que ponerse el RELE 3
@note Si existe algun cambio de configuracion de los reles, se enviará un paquete
y este se modificará en funcion de la vaqueriza y la zona
*/
void preparaPaqueteRele (uint32_t chip_id, String marcador, String ID_Vaqueriza, String ID_ZONA, String a_m, String Relex, String Reley, String Relez, String Relet){
  T_EIC = chip_id;
  paqueteSalida = String(chip_id, HEX) + " " + marcador + " " + ID_Vaqueriza + " " + ID_ZONA + " " + a_m + " " + Relex + " " + Reley + " " + Relez + " " + Relet + " " + String(T_EIC, HEX) + " ";
  if(DBUG == 1){
    Serial.println("El paquete de estado de los reles preparado para su salida es: " + paqueteSalida);
  }
}
/**
Funcion prepara un paquete de configuracion para envio LoRa
@param chip_id el valor del serial number (32bits) del dispositivo que envia
@param marcador tipo de paquete que va a ser 
@param ID_Vaqueriza	el ID de la vaqueriza correspondiente a ese microcontrolador
@param ID_ZONA	el ID de la zona correspondiente a ese microcontrolador
@param RealID el numero de serie real (64 bits) de ese microcontrolador   
@note Si se envia un health bit el cual no tiene una vaqueriza y zona configurada, el concentrador
busca en la base de datos y manda al microcontrolador su vaqueriza y zona.
*/
void preparaPaqueteConfiguracion (uint32_t chip_id, String marcador, String ID_Vaqueriza, String ID_ZONA, String RealID){
	T_EIC = chip_id;
	paqueteSalida = String(chip_id, HEX) + " " + marcador + " " + ID_Vaqueriza + " " + ID_ZONA + " " + RealID + " " + String(T_EIC, HEX) + " ";
	if(DBUG == 1){
    Serial.println("El paquete de configuracion que esta saliendo es: " + paqueteSalida);
  }
}
/**
Funcion prepara un paquete ACK para envio LoRa
@param chip_id el valor del serial number (32bits) del nodo que envia
@param marcador tipo de paquete que va a ser 
@param ID_Vaqueriza	el ID de la vaqueriza en la que se encuentra
@param ID_ZONA	el ID de la zona en la que se encuentra
@param OK una cadena que identifica que puede continuar con el siguiente paquete   
@note Si al recibir el HEALTH BIT esta todo correcto, mandará este paquete para indicar que mande el paquete de datos
*/
void prepararACK (uint32_t chip_id, String marcador, String ID_Vaqueriza, String ID_ZONA, String OK){
	T_EIC = chip_id;
	paqueteSalida = String(chip_id, HEX) + " " + marcador + " " + ID_Vaqueriza + " " + ID_ZONA + " " + OK + " " + String(T_EIC, HEX) + " ";
	if(DBUG == 1){
    Serial.println("El paquete de ACK que esta saliendo es: " + paqueteSalida);
  }
}
/**
Funcion prepara un paquete HEALTH BIT para envio LoRa
@param chip_id el valor del serial number (32bits) del nodo que envia
@param marcador tipo de paquete que va a ser 
@param ID_Vaqueriza	el ID de la vaqueriza en la que se encuentra
@param ID_ZONA	el ID de la zona en la que se encuentra
@param RealID su Serial number real   
@param bateria El estado en el que se encuentra la batería
@note Cada vez que se vaya a enviar un paquete de datos se manda un HEALTH BIT para comprobar que todo esta OK
*/
void prepararHEALTHBit(uint32_t chip_id, String marcador, String ID_Vaqueriza, String ID_ZONA, String RealID ,float bateria){
	T_EIC = chip_id;
	paqueteSalida = String(chip_id, HEX) + " " + marcador + " " + ID_Vaqueriza + " " + ID_ZONA + " " + RealID + " "+ String(bateria) + " " + String(T_EIC, HEX) + " ";
	if(DBUG == 1){
    Serial.println("El paquete de HEALTH BIT que esta saliendo es: " + paqueteSalida);
  }
}
/**
Funcion prepara un paquete TRY AGAIN para envio LoRa
@param chip_id el valor del serial number (32bits) del dispositivo que envia
@param marcador tipo de paquete que va a ser 
@param ID_Vaqueriza	el ID de la vaqueriza en la que se encuentra
@param ID_ZONA	el ID de la zona en la que se encuentra
@param intentar String que indica que hay que volver a mandar el último paquete   
@note Si hay algun paquete que reciba el concentrador, el cual no tiene sentido o no tiene integridad, manda un mensaje para que los nodos
vuelvan a mandar el healthbit y comience la comunicacion.
*/
void preparaTryAgain(uint32_t chip_id, String marcador, String ID_Vaqueriza, String ID_ZONA, String intentar){
	T_EIC = chip_id;
	paqueteSalida = String(chip_id, HEX) + " " + marcador + " " + ID_Vaqueriza + " " + ID_ZONA + " " + intentar + " " + String(T_EIC, HEX) + " ";
	if(DBUG == 1){
    Serial.println("El paquete de Try Again que esta saliendo es: " + paqueteSalida);
  }
}
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																						CALCULAR OR || VERIFICAR RELES || DESCOMPONER PAQUETE                                                  //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

/**
Funcion que comprueba la integridad del dato
@returns bool para comprobar si hay o no integridad en el paquete
@note Si el serial number de 32 bit recibido es igual al codigo R_EIC (que debe de ser igual)
devolveremos un 1, en caso de no serlo se devolvera un 0.
*/
bool calcularOR(){
	if ((R_chipid) == R_EIC){
		if (DBUG == 1){
		Serial.println("El dato tiene la suficiente integridad");
		}
		return 1;
	}
	else {
		Serial.println("El dato NO TIENE integridad");
		Serial.println("R_chipid: " + R_chipid);
		Serial.print("R_EIC: " + R_EIC);
		return 0;
	}
}
// SIEMPRE QUE EL ID DE LA VAQUERIZA QUE HA RECIBIDO EL PAQUETE SEA IGUAL QUE LA SUYA PROPIA
// Y LA ZONA RECIBIDA SEA IGUAL QUE LA SUYA PROPIA 
// TOMARÁ EL VALOR DE MANUAL Y ACTIVARÁ LOS RELÉS QUE SE LE DIGAN
// SI ESTA EN MODO AUTOMÁTICO, NO HARÁ CASO A ESTA OPERACIÓN
/**
Funcion que verifica el estado de los reles en caso de ser necesario.
@returns devolverá 1 si ha existido algun cambio, 0 si no ha habido ningun cambio.
@note Le asigna el valor recibido de el estado de auto o manual a la variable propia del dispositivo,
y activará y desactivará los relés en función de los escrito en la base de datos.
*/
bool verificaReles(){
	if (DBUG == 1){
		Serial.println("Estoy verificando los reles..");
	}
	if (T_ID_ZONA == R_ID_ZONA){	
		if (DBUG == 1){
			Serial.println("La zona que tiene que cambiar es la mia");
		}
		T_auto_manual = R_auto_manual.toInt();
		if (R_auto_manual == "0"){
			if ((R_StatusR0 == "1") ){//&& (R_StatusR0 != R_StatusR0V)){
				des_activaRele0(1);
			}
			if ((R_StatusR0 == "0") ){//&& (R_StatusR0 != R_StatusR0V)){
				des_activaRele0(0);
			}
			if ((R_StatusR1 == "1") ){//&& (R_StatusR1 != R_StatusR1V)){
				des_activaRele1(1);
			}
			if ((R_StatusR1 == "0") ){//&& (R_StatusR1 != R_StatusR1V)){
				des_activaRele1(0);
			}
			if ((R_StatusR2 == "1") ){//&& (R_StatusR2 != R_StatusR2V)){
				des_activaRele2(1);
			}
			if ((R_StatusR2 == "0") ){//&& (R_StatusR2 != R_StatusR2V)){
				des_activaRele2(0);
			}
			if ((R_StatusR3 == "1") ){//&& (R_StatusR3 != R_StatusR3V)){
				des_activaRele3(1);
			}
			if ((R_StatusR3 == "0") ){//&& (R_StatusR3 != R_StatusR3V)){
				des_activaRele3(0);
			}
		}
		return 1;
	}
	return 0;
}
/**
Funcion que configura la vaqueriza y la zona, en caso de que el nodo no la tenga asignada.
@note Si el numero de serie (64 bits) del dispositivo corresponde con el transmitido por el concentrador,
ese dispotivo pasara a asignarse su zona y si vaqueriza.
*/
void configuraVaqueriaZona(){
	if (T_StringRealID == R_StringRealID){
		T_ID_VAQUERIZA = R_ID_VAQUERIZA;
		T_ID_ZONA = R_ID_ZONA;
		if (DBUG == 1){
			Serial.println("Mi ID_Vaqueria es: " + T_ID_VAQUERIZA + "\nMi ID_Zona es: " + T_ID_ZONA);
			errorOLED("Zona y Vaqueriza Configurada");
		}
	}
}

																																/* --- RECEPCIÓN --- */

// LA SIGUIENTE FUNCIÓN DESCOMPONDRÁ EL PAQUETE RECIBIDO POR LORA EN EL DISPOSITIVO
// TENEMOS DISTINTOS TIPOS DE PAQUETE, INDICADOS POR EL MARCADOR
// LOS CUATRO PRIMEROS CAMPOS SON LOS QUE INDICAN QUE DISPOSITIVO ES (CHIPID)
// QUE TIPO DE PAQUETE ENVIA (MARCADOR)
// LA VAQUERIZA Y LA ZONA EN LA QUE SE ENCUENTRAN (ID_VAQUERIZA, ID_ZONA)

	// SI EL MARCADOR ES UN 0, EL PAQUETE LLEVARÁ LA SIGUIENTE ESTRUCTURA:
			//
			//

	// SI EL MARCADOR ES UN 1, EL PAQUETE LLEVARÁ LA SIGUIENTE ESTRUCTURA:
			// TEMPERATURA, HUMEDAD, ITH, ESTADO DE LOS RELÉS
			// HUMEDAD será un valor del 00.00 al 99.99
			// TEMPERATURA será un valor del 00.00 al 99.99
			// ITH será un valor entero del 0 al 100
			// R0, R1, R2 y R3 serán valores booleanos, 1 indicará encendido y 0 indicará apagado
			// EIC CÓDIGO QUE LE DA INTEGRACIÓN AL PAQUETE

	// SI EL MARCADOR ES UN 2, EL PAQUETE LLEVARÁ LA SIGUIENTE ESTRUCTURA:
			// AUTO_MANUAL, Variable que indicará con un 1 automático y con un 0 manual
			// R0, R1, R2 y R3 serán valores booleanos, 1 indicará puesta a encendido y 0 indicará puesta a apagado
			// EIC CÓDIGO QUE LE DA INTEGRACIÓN AL PAQUETE
	// SI EL MARCADOR ES UN 3, EL PAQUETE LLEVARÁ LA SIGUIENTE ESTRUCTURA:
			// R_chipid QUE SERÁ AL DISPOSITIVO QUE SE LE ASIGNARÁ LA VAQUERÍA Y ZONA
/**
Funcion encargada de descomponer el paquete LoRa recibido
con el fin de asignarle el valor a las variables que se encuentran en el paquete
@param cadena la variable de tipo String que se va a descomponer
@note Las dos primeras variables son comunes para todos los paquetes, el numero de serie del dispositivo que manda (32 bits)
y el marcador que es el que identifica el tipo de paquete que se ha recibido, en función de este último descompondremos de una manera
u otra el paquete recibido.
- Marcador es un 0 (HEALTH BIT): Vaqueriza y zona desde la que se manda, número de serie (64 bits), nivel de batería y código de integridad de errores.
- Marcador es un 1 (INFORMACION DATOS): Vaqueriza y zona desde la que se manda, temperatura, humedad, ITH y el estado de los relés y código de integridad de errores.
- Marcador es un 2 (Automatico/Manual): Vaqueriza y zona a la que va el paquete, bit de auto o manual, estado al que hay que poner los relés y código de integridad de errores.
- Marcador es un 3 (CONFIGURACION): Vaqueriza, zona y numero de serie, la vaqueriza y la zona serán las correspondientes a ese numero de serie y código de integridad de errores.
- Marcador es un 4 (TODO OK/ACK): Vaqueriza y zona al que va el paquete, ACK indicanto todo OK, y código de integridad de errores.
- Marcador es un 5 (Try Again): Vaqueriza y zona desde la que se manda, mensaje Try Again y código de integridad de errores.
*/
void descomponerPaquete (String cadena){
	R_chipid = s.separa(cadena, ' ' , 0);
  R_marcador = s.separa(cadena, ' ' , 1);
	if (DBUG == 1){
  Serial.println("Descomponiendo el paquete ...");
  Serial.println("El valor de R_chipid es: " + R_chipid);
  Serial.println("El valor de marcador vale: " + R_marcador);
	}
	switch (R_marcador.toInt()){
    case 0:
			if (concentrador == 0){
				break;
			}
      //HEALT BIT
			R_ID_VAQUERIZA = s.separa(cadena, ' ', 2);
			R_ID_ZONA = s.separa(cadena, ' ', 3);
			R_StringRealID = s.separa(cadena, ' ', 4);
			R_levelBateria = s.separa(cadena, ' ', 5);
		  R_EIC = s.separa(cadena, ' ', 6);
			if (DBUG == 1){
				Serial.println("El valor de Vaqueria: " + R_ID_VAQUERIZA);
				Serial.println("El valor de Zona: " + R_ID_ZONA);
				Serial.println("El numero de serie es: " + R_StringRealID);
				Serial.println ("El valor de la bateria es: " + R_levelBateria);
				Serial.println("El valor de codigo es: " + R_EIC);
			}
      break;
    case 1:
      //INFORMACION DATOS      
			if (concentrador == 0){
				break;
			}
			recibidoNodo = 1;
			R_ID_VAQUERIZA = s.separa(cadena, ' ', 2);
			R_ID_ZONA = s.separa(cadena, ' ', 3);
			R_temp = s.separa(cadena, ' ', 4);
      R_hum = s.separa(cadena, ' ', 5);
      R_ITH = s.separa(cadena, ' ', 6);
      R_StatusR0 = s.separa(cadena, ' ', 7);
      R_StatusR1 = s.separa(cadena, ' ', 8);
      R_StatusR2 = s.separa(cadena, ' ', 9);
      R_StatusR3 = s.separa(cadena, ' ', 10);
			R_levelBateria = s.separa(cadena, ' ', 11);
      R_EIC = s.separa(cadena, ' ', 12);
      if (DBUG == 1){
				Serial.println("El valor de Vaqueria: " + R_ID_VAQUERIZA);
				Serial.println("El valor de Zona: " + R_ID_ZONA);
				Serial.println("El valor de temperatura: " + R_temp);
				Serial.println("El valor de humedad: " + R_hum);
				Serial.println("El valor de ITH: " + R_ITH);
				Serial.println("El valor de R0: " + R_StatusR0);
				Serial.println("El valor de R1: " + R_StatusR1);
				Serial.println("El valor de R2: " + R_StatusR2);
				Serial.println("El valor de R3: " + R_StatusR3);
				Serial.println("El valor de bateria: " + R_levelBateria);
				Serial.println("El valor de codigo es: " + R_EIC);
      }
      break;
    case 10:
			if (concentrador == 1){
				break;
			}
			//AUTOMATICO/MANUAL ESTADO DE LOS RELÉS
			R_ID_VAQUERIZA = s.separa(cadena, ' ', 2);
			R_ID_ZONA = s.separa(cadena, ' ', 3);
			R_auto_manual = s.separa(cadena, ' ', 4);
			R_StatusR0 = s.separa(cadena, ' ', 5);
			R_StatusR1 = s.separa(cadena, ' ', 6);
			R_StatusR2 = s.separa(cadena, ' ', 7);
			R_StatusR3 = s.separa(cadena, ' ', 8);
		  R_EIC = s.separa(cadena, ' ', 9);
			if (DBUG == 1){
				Serial.println("El valor de Vaqueria: " + R_ID_VAQUERIZA);
				Serial.println("El valor de Zona: " + R_ID_ZONA);
				Serial.println("El valor de R0: " + R_StatusR0);
				Serial.println("El valor de R1: " + R_StatusR1);
				Serial.println("El valor de R2: " + R_StatusR2);
				Serial.println("El valor de R3: " + R_StatusR3);
				Serial.println("El valor de codigo es: " + R_EIC);
			}
      break;
		case 11:
			if (concentrador == 1){
				break;
			}
			// CONFIGURACION DE LA VAQUERÍA Y LA ZONA PARA UN DISPOSTIVO EN CONCRETO CON SU CHIP ID
			R_ID_VAQUERIZA = s.separa(cadena, ' ', 2);
			R_ID_ZONA = s.separa(cadena, ' ', 3);
			R_StringRealID = s.separa(cadena, ' ', 4);
			R_EIC = s.separa(cadena, ' ', 5);
			if (DBUG == 1){
				Serial.println("El valor de Vaqueria: " + R_ID_VAQUERIZA);
				Serial.println("El valor de Zona: " + R_ID_ZONA);
				Serial.println("El valor del chip al que va el dato es: " + R_chipid);
				Serial.println("El valor de codigo es: " + R_EIC);
			}
			break;
		case 100:
			// TODO ESTA CORRECTO
			if (concentrador == 1){
				break;
			}
			R_ID_VAQUERIZA = s.separa(cadena, ' ', 2);
			R_ID_ZONA = s.separa(cadena, ' ', 3);
			R_ACK = s.separa(cadena, ' ', 4);
			R_EIC = s.separa(cadena, ' ', 5);
			if (DBUG == 1){
				Serial.println("El valor de Vaqueria: " + R_ID_VAQUERIZA);
				Serial.println("El valor de Zona: " + R_ID_ZONA);
				Serial.println("El valor del mensaje del ACK es: " + R_ACK);
				Serial.println("El valor de codigo es: " + R_EIC);
			}
			break;
    case 101:
			// VUELVE A MANDAR EL PAQUETE
			if (concentrador == 1){
				break;
			}
			R_ID_VAQUERIZA = s.separa(cadena, ' ', 2);
			R_ID_ZONA = s.separa(cadena, ' ', 3);
			R_tryAgain = s.separa(cadena, ' ', 4);
			R_EIC = s.separa(cadena, ' ', 5);
			if (DBUG == 1){
				Serial.println("El valor de Vaqueria: " + R_ID_VAQUERIZA);
				Serial.println("El valor de Zona: " + R_ID_ZONA);
				Serial.println("El valor de Try Again es: " + R_tryAgain);
				Serial.println("El valor de codigo es: " + R_EIC);
			}
			break;
		default:
      Serial.println("marcador incorrecto");
      break;
    }
}
/**
Funcion que borra el valor de las variables usadas para descomponer el paquete.
@note Esta funcion es llamada una vez que se ha descompuesto el paquete de entrada, y se a actuado 
en funcion de las variables utilizadas y su valor (marcador indica que tipo de paquete es el de entrada).
*/
void limpiarVariables(){
	R_chipid = "";
	R_marcador = "";
	R_ID_VAQUERIZA = "";
	R_ID_ZONA = "";
	R_StringRealID = "";
	R_levelBateria = "";
	R_EIC = "";
	R_temp = "";
	R_hum = "";
	R_ITH = "";
	R_StatusR0 = "";
	R_StatusR1 = "";
	R_StatusR2 = "";
	R_StatusR3 = "";
	R_auto_manual = "";
	R_ACK = "";
	R_tryAgain = "";	
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										PAQUETE LORA						                                                                       //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//RECEPCION DE PAQUETES DEL CONCENTRADOR Y NODOS.
/**
Funcion que se encarga realizar una acción u otra en función del tipo de marcador.
@note Esta función comprueba cual es el tipo de marcador y que el dato tiene integridad,
despues de esto, sigue la estructura normal de la comunicacion:
- Si recibimos un health bit, le pedimos los datos al nodo (con configuracion de zona y vaqueriza o no)
- Si recibimos un paquete de datos, los subimos a la base de datos
- si recibimos un paquete de cambio de estado de los reles, verificamos el cambio
- si recibimos un paquete de configuracion, actualizamos vaqueriza y zona y mandamos los datos
- Si recibimos un paquete de todo correcto, mandamos el paquete de datos
- Si recibimos un tray again, volvemos a mandar el ultimo paquete.
*/
void paqueteLoRa(){
		descomponerPaquete(paqueteEntrada);
		R_EIC.trim();
		comprobar = calcularOR();
		paqueteEntrada = "";
		if (R_marcador == "0000" && comprobar == 1){			// SI EL MARCADOR ES UN 0, HEALTH BIT ENVIADO DEL NODO AL CONCENTRADOR
			if (R_ID_VAQUERIZA == "-" && R_ID_ZONA == "-"){
				actualizarDatos(1);
				tipoPaquete(3);
				if (T_ID_VAQUERIZA == R_ID_VAQUERIZA){
					preparaPaqueteConfiguracion(chipid, T_marcador, R_ID_VAQUERIZA, R_ID_ZONA, R_StringRealID);
					enviaPaquete(paqueteSalida);			
				}
			}
			else{
				if (T_ID_VAQUERIZA == R_ID_VAQUERIZA){
					tipoPaquete(4);													// ESTE MANDA UN PAQUETE DICIENDO QUE TODO ESTÁ OK, Y QUE LE DE DATOS
					prepararACK(chipid, T_marcador, R_ID_VAQUERIZA, R_ID_ZONA, T_ACK);
					enviaPaquete(paqueteSalida);
				}
			}
		}
    if (R_marcador == "0001" && comprobar == 1){			// SI EL MARCADOR ES UN 1, PAQUETE DE DATOS ENVIADO DE UN NODO AL CONCENTRADOR,
				if (T_ID_VAQUERIZA == R_ID_VAQUERIZA){
					String PaqueteURL = paqueteURL(recibidoNodo);
					hacerConexion(true);										// ESTE SUBE LOS DATOS A LA BASE DE DATOS
					envioDatosURL(PaqueteURL);
				}
		}
		if (R_marcador == "0010" && comprobar == 1){			// SI EL MARCADOR ES UN 2, HAY QUE MIRAR SI PASAMOS A MODO MANUAL Y SI HAY QUE ACTIVAR ALGUN RELE
			verificaReles();
		}
		if (R_marcador == "0011" && comprobar ==1){
			configuraVaqueriaZona();								// SI EL MARCADOR ES UN 3, HAY QUE CONFIGURAR LA ID DE ZONA Y LA VAQUERIZA
			if ((T_ID_VAQUERIZA == R_ID_VAQUERIZA) && (T_ID_ZONA == R_ID_ZONA)){
				tipoPaquete(1); 
				preparaPaqueteDatos(chipid, T_marcador, T_ID_VAQUERIZA, T_ID_ZONA, T_temp, T_hum, T_ITH, T_StatusR0, T_StatusR1, T_StatusR2, T_StatusR3, T_levelBateria);
				enviaPaquete(paqueteSalida);
			}
			if (bateria == 1){
				tareaCumplida = 1;
			}
			if(DBUG == 1){
				Serial.println("Nos encontramos en modo NODO, hemos configurado la zona y hemos preparado y enviado el paquete");
				}
		}
		if (R_marcador == "0100" && comprobar ==1){			// SI EL MARCADOR ES UN 4, DEBEMOS ENVIAR LA INFORMACION DE LOS SENSORES
			if ((T_ID_VAQUERIZA == R_ID_VAQUERIZA) && (T_ID_ZONA == R_ID_ZONA)){
				esperaPaquete = 1;
				tipoPaquete(1); 
				preparaPaqueteDatos(chipid, T_marcador, T_ID_VAQUERIZA, T_ID_ZONA, T_temp, T_hum, T_ITH, T_StatusR0, T_StatusR1, T_StatusR2, T_StatusR3, T_levelBateria);
				enviaPaquete(paqueteSalida);	
			}
			if (bateria == 1){
				tareaCumplida = 1;
			}
			if(DBUG == 1){
				Serial.println("Nos encontramos en modo NODO y hemos preparado y enviado el paquete");
				}
		}
		if (R_marcador == "0101" && comprobar == 1){		//SI EL MARCADOR ES UN 5 DEBEMOS VOLVER A ENVIAR EL ÚLTIMO PAQUETE ENVIADO
			if (T_ID_ZONA == "00"){
				return;
			}
			if (T_ID_ZONA == "01"){
				enviaHealthBit();
			}
			if (T_ID_ZONA == "02"){
				delay(1000);
				enviaHealthBit();
			}
			if (T_ID_ZONA == "03"){
				delay(2000);
				enviaHealthBit();
			}
			if (T_ID_ZONA == "04"){
				delay(3000);
				enviaHealthBit();
			}
		}
		if (comprobar == 0 && concentrador == 1){
			//tipoPaquete(5);
			//preparaTryAgain(chipid, T_marcador, T_ID_VAQUERIZA, T_ID_ZONA, "TRY");
			//enviaPaquete(paqueteSalida);
		}
		limpiarVariables();		
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										envia Health Bit                                                                     //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que establece la comunicacion con el concentrador.
@note Esta función solo se ejecutará en el flujo de programa de los nodos, y la bandera para que se pueda activar y 
por lo tanto volver a ejecutar la función la pone a uno el timer3 (20min para el envio de datos)
*/
void enviaHealthBit (){
	if (healthBit == 0) {
		tipoPaquete(0);
		if (bateria == 1){
			leerBateria();
		}
    prepararHEALTHBit(chipid, T_marcador, T_ID_VAQUERIZA, T_ID_ZONA, T_StringRealID, T_levelBateria);
    enviaPaquete(paqueteSalida);
    healthBit = 1;
   }
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										MODO SUSPENSIÓN                                                                        //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//FUNCION PARA PONER EL MICROCONTROLADOR EN MODO SUEÑO PROFUNDO
/**
Funcion pone en modo sueño profundo al microcontrolador.
@note El timepo esta definido en los TIMERS
*/
void Adormir(){
	LoRa.end();
  LoRa.sleep();
  delay(100);
	pinMode(5, INPUT);
  pinMode(14, INPUT);
  pinMode(15, INPUT);
  pinMode(16, INPUT);
  pinMode(17, INPUT);
  pinMode(18, INPUT);
  pinMode(19, INPUT);
  pinMode(26, INPUT);
  pinMode(27, INPUT);
  esp_sleep_enable_timer_wakeup(minutos20);
	if (DBUG == 1){
		Serial.println("Setup ESP32 to sleep for every " + String(minutos20) + " uSeconds");
		Serial.println("Going to sleep now");
	}
	errorOLED("NOS VAMOS A DORMIR");
  esp_deep_sleep_start();  
  }


/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										ACTUALIZAR DATOS DEL MICRO                                                                  //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
void actualizarDatos(bool exterior){
	hacerConexion(1);
	String auxPaquete = "/accederConfiguracion.php?SerialNumber="; 
	if (exterior == 1){
		auxPaquete = auxPaquete + R_StringRealID;
	}
	else{
		auxPaquete = auxPaquete + T_StringRealID;
	}
	if(DBUG == 1){
		Serial.println(auxPaquete);		
	}
	client.print(String("GET ") + auxPaquete + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n");
	unsigned long timeout = millis();
	while (client.available() == 0) {
		if (millis() - timeout > 5000) {
			if (DBUG == 1){
				Serial.println(">>> Client Timeout !");
			}
			client.stop();
			desconectarWifi();
			conectarWifi();
			actualizarDatos(exterior);
			return;
		}
	}
		// Read all the lines of the reply from server and print them to Serial
	while (client.available()) {
		String line = client.readStringUntil('\r');
		if (line.substring(line.indexOf('t'),line.indexOf('t')+12) == "tusdatosson:") {
			auxConfiguracionVaquerizaZona = line.substring(line.indexOf(':')+1,line.length());
			Serial.println(auxConfiguracionVaquerizaZona);
		}
		if (DBUG==1){
			Serial.print(line);
		}
	}
	if (exterior == 1){
	descomponerConfiguracionBBDD(1,auxConfiguracionVaquerizaZona);
	}
	else{
	descomponerConfiguracionBBDD(0,auxConfiguracionVaquerizaZona);
	
	}
	if (DBUG == 1){
		Serial.println();
		Serial.println("closing connection Configuracion Vaqueriza y Zona");
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										DESCOMPONER LOS DATOS DE CONFIGURACION RECOGIDOS DE LA BBDD                                    //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

void descomponerConfiguracionBBDD(bool exterior, String datos){
	if (exterior == 1){
		R_ID_VAQUERIZA = s.separa(datos, ' ', 0);
		R_ID_ZONA = s.separa(datos, ' ', 1);
		R_ID_ZONA.trim();
	}
	else{
		T_ID_VAQUERIZA = s.separa(datos, ' ',0);
		T_ID_ZONA = s.separa(datos, ' ', 1);
		T_ID_ZONA.trim();
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										ACTUALIZAR DATOS DE LOS RELES                                                                  //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

void comprobarAutoManual(){
	hacerConexion(1);
	String auxPaquete = "/configuracionRele.php?ID_VAQUERIZA=" + T_ID_VAQUERIZA;
	if(DBUG == 1){
		Serial.println(auxPaquete);		
	}
	client.print(String("GET ") + auxPaquete + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n");
	unsigned long timeout = millis();
	while (client.available() == 0) {
		if (millis() - timeout > 5000) {
			if (DBUG == 1){
				Serial.println(">>> Client Timeout !");
			}
			client.stop();
			return;
		}
	}
		// Read all the lines of the reply from server and print them to Serial
	while (client.available()) {
		String line = client.readStringUntil('\r');
		if (line.substring(line.indexOf('t'),line.indexOf('t')+12) == "tusdatosson:") {
			auxConfiguracionAutoManual = line.substring(line.indexOf(':')+1,line.length());
			Serial.println(auxConfiguracionAutoManual);
		}
		if (DBUG==1){
			Serial.print(line);
		}
	}
	descomponerConfiguracionAutoManual(auxConfiguracionAutoManual);
	if (DBUG == 1){
		Serial.println();
		Serial.println("closing connection Configuracion Rele");
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										DESCOMPONER LOS DATOS DE CONFIGURACION RECOGIDOS DE LA BBDD                                    //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

void descomponerConfiguracionAutoManual (String dato){
	R_ID_ZONA = s.separa(dato, ' ', 0);
	R_auto_manual = s.separa(dato, ' ', 1);
	R_StatusR0 = s.separa(dato, ' ', 2);
	R_StatusR1 = s.separa(dato, ' ', 3);
	R_StatusR2 = s.separa(dato, ' ', 4);
	R_StatusR3 = s.separa(dato, ' ', 5);
	R_StatusR3.trim();
	if (DBUG == 1){
		Serial.println("El valor de Zona es: " + R_ID_ZONA);
		Serial.println("El valor de AutoManual es: " + R_auto_manual);
		Serial.println("El valor de R0 es: " + R_StatusR0);
		Serial.println("El valor de R1 es: " + R_StatusR1);
		Serial.println("El valor de R2 es: " + R_StatusR2);
		Serial.println("El valor de R3 es: " + R_StatusR3);
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										COMPROBAR QUE HAN CAMBIADO EL ESTADO DE LOS RELÉS					                                     //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

bool comprobarCambiosRele(String zona){
	if (zona == T_ID_ZONA){
		bool auxcomprobar = verificaReles();
		return auxcomprobar;
	}
	if (zona != T_ID_ZONA){
		if (DBUG == 1){
			Serial.println("Pasamos a comprobar si hay cambios..");
		}
		if ((R_auto_manual != auto_manualV) || (R_StatusR0 != R_StatusR0V) || (R_StatusR1 != R_StatusR1V) || (R_StatusR2 != R_StatusR2V) || (R_StatusR3 != R_StatusR3V)){
			tipoPaquete(2);
			preparaPaqueteRele (chipid, T_marcador, T_ID_VAQUERIZA, R_ID_ZONA, R_auto_manual, R_StatusR0, R_StatusR1, R_StatusR2, R_StatusR3);
			enviaPaquete(paqueteSalida);
			return 1;
		}
/*		if (R_StatusR0 != R_StatusR0V){
			tipoPaquete(2);
			preparaPaqueteRele (chipid, T_marcador, T_ID_VAQUERIZA, R_ID_ZONA, R_auto_manual, R_StatusR0, R_StatusR1, R_StatusR2, R_StatusR3);
			enviaPaquete(paqueteSalida);
			return 1;
		}
		if (R_StatusR1 != R_StatusR1V){
			tipoPaquete(2);
			preparaPaqueteRele (chipid, T_marcador, T_ID_VAQUERIZA, R_ID_ZONA, R_auto_manual, R_StatusR0, R_StatusR1, R_StatusR2, R_StatusR3);
			enviaPaquete(paqueteSalida);
			return 1;
		}
		if (R_StatusR2 != R_StatusR2V){
			tipoPaquete(2);
			preparaPaqueteRele (chipid, T_marcador, T_ID_VAQUERIZA, R_ID_ZONA, R_auto_manual, R_StatusR0, R_StatusR1, R_StatusR2, R_StatusR3);
			enviaPaquete(paqueteSalida);
			return 1;
		}
		if (R_StatusR2 != R_StatusR2V){
			tipoPaquete(2);
			preparaPaqueteRele (chipid, T_marcador, T_ID_VAQUERIZA, R_ID_ZONA, R_auto_manual, R_StatusR0, R_StatusR1, R_StatusR2, R_StatusR3);
			enviaPaquete(paqueteSalida);
			return 1;
		}
		if (R_StatusR3 != R_StatusR3V){
			tipoPaquete(2);
			preparaPaqueteRele (chipid, T_marcador, T_ID_VAQUERIZA, R_ID_ZONA, R_auto_manual, R_StatusR0, R_StatusR1, R_StatusR2, R_StatusR3);
			enviaPaquete(paqueteSalida);
			return 1;
		}*/
	}
	else{
		return 0;
	}
}
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										ACTUALIZAR LOS ESTADOS DE LOS RELES PARA COMPROBAR SI HAY CAMBIO                              //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

void actualizarEstadosRele(){
	auto_manualV = R_auto_manual;
	R_StatusR0V = R_StatusR0;
	R_StatusR1V = R_StatusR1;
	R_StatusR2V = R_StatusR2;
	R_StatusR3V = R_StatusR3;
	if (DBUG == 1){
		Serial.println("Las variables han sido actualizadas");
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										FUNCION QUE ENGLOBA A LAS OTRAS, PARA EJECUTAR SOLO UNA 	                                     //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

void actualizarAutoManualRele(){
	comprobarAutoManual();
	bool aux = comprobarCambiosRele(R_ID_ZONA);
	if (aux == 1){
		actualizarEstadosRele();
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										EXTRAER USUARIO Y CONTRASEÑA DEL WIFI AL QUE VAMOS A CONECTAR                                  //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que obtiene el SSID y PASSWORD de la granja a conectarse.
@note Nos conectamos contra la base de datos con el fin de obtener con su numero de serie, el SSID y PASSWORD del
router a conectarse. Esta cadena de texto es almacenada en una variable y se descompone.
*/
void extraerSsidPassword(){
	hacerConexion(1);
	String auxPaquete = "/configuracionWifi.php?SerialNumber=" + T_StringRealID;
	if(DBUG == 1){
		Serial.println(auxPaquete);		
	}
	String auxConfiguracionTemporal;
	client.print(String("GET ") + auxPaquete + " HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n");
	unsigned long timeout = millis();
	while (client.available() == 0) {
		if (millis() - timeout > 5000) {
			if (DBUG == 1){
				Serial.println(">>> Client Timeout !");
			}
			client.stop();
			return;
		}
	}
		// Read all the lines of the reply from server and print them to Serial
	while (client.available()) {
		String line = client.readStringUntil('\r');
		if (line.substring(line.indexOf('t'),line.indexOf('t')+12) == "tusdatosson:") {
			auxConfiguracionTemporal = line.substring(line.indexOf(':')+1,line.length());
			Serial.println(auxConfiguracionTemporal);
		}
		if (DBUG==1){
			Serial.print(line);
		}
	}
	descomponerConfiguracionTemporal(auxConfiguracionTemporal);
	if (DBUG == 1){
		Serial.println();
		Serial.println("closing connection Configuracion Wifi");
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										DESCOMPONER SSID Y PASSWORD DEVUELTOS EN HTML									                                 //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que descompone la cadena de texto que contiene SSID y PASSWORD de la granja
@param String que es la cadena de texto a descomponer.
@note SSID y PASSWORD se le asocia a ssid y password para la posterior conexion.
*/
void descomponerConfiguracionTemporal(String dato){
	ssidB = s.separa(dato, ' ', 0);
	passwordB = s.separa(dato, ' ', 1);
	passwordB.trim();
	ssidB.toCharArray(buf1, leng1);
	ssid = buf1;
	passwordB.toCharArray(buf2,leng2);
	password = buf2;
	if (DBUG == 1){
		Serial.println(buf1);
		Serial.println(buf2);
		Serial.println(ssid);
		Serial.println(password);	
		Serial.println("ssid al que conectamos: " + ssidB);
		Serial.println("password con la que conectamos: " + passwordB);
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										INICIAR WIFI TEMPORAL																				                                   //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que nos facilita la conexion incial.
@note Conectamos a un wifi generico, pasamos a obtener nuestro verdadero SSID y PASSWORD y lo guardamos en las variables oportunas.
*/
void iniciarWifiTemporal(){
	conectarTemporal();
	extraerSsidPassword();
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										ESCRIBIR EN LA EEPROM SSID Y PASSWORD												                                  //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que ESCRIBE en la EEPROM.
@note Escribimos en la EEPROM lo que necesitamos, SSID y PASSWORD de la granja.
*/
void enviarDatosEEPROM(){
	i2c_eeprom_write_page(0x53, 2, (byte *)buf1, sizeof(buf1));
	i2c_eeprom_write_page(0x53, 27, (byte *)buf2, sizeof(buf2));
	if (DBUG == 1){
	Serial.println("SSID y PASSWORD escritos en la EEPROM");
	}
}
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										PREGUNTAR SI LA EEPROM CONTIENE LOS DATOS DEL SSID Y PASSWORD                                  //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que consulta en la EEPROM.
@returns bool que devolverá un cero si la EEPROM esta vacía, y un 1 si hay dato.
@note Consultamos previamente en la dirección de la EEPROM, y leemos de la EEPROM los lugares donde
hemos escrito anteriormente.
*/
bool consultarDatosEEPROM(){
	Serial.println("Consultando datos de la EEPROM");
	String auxSsid="";
	String auxPassword="";
	int addr = 2;
	byte b = i2c_eeprom_read_byte(0x53, addr);
	while (b != 0){
		auxSsid += (char)b;
		addr ++;
		b = i2c_eeprom_read_byte(0x53, addr);		
	}
	addr = 27;
	byte c = i2c_eeprom_read_byte(0x53, addr);
	while (c != 0){
		auxPassword += (char)c;
		addr ++;
		c = i2c_eeprom_read_byte(0x53, addr);
	}
	if (auxSsid.length() < 2 || auxPassword.length() <2 ){
		if (DBUG == 1){
			Serial.println("La EEPROM esta vacia");
		}
		return 0;
	}
	else{
		auxSsid.toCharArray(buf1, leng1);
		ssid = buf1;
		auxPassword.toCharArray(buf2,leng2);
		password = buf2;
		Serial.println("Datos detectados en al EEPROM");
		return 1;
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
//																										ARRANCAR LA PLACA Y VERIFICAR QUE LA EEPROM CONTIENE O NO DATOS                                //
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
Funcion que engloba a las anteriores.
@returns devolverá 1 si ha existido algun cambio, 0 si no ha habido ningun cambio.
@note Le asigna el valor recibido de el estado de auto o manual a la variable propia del dispositivo,
y activará y desactivará los relés en función de los escrito en la base de datos.
*/
void inicializacionBasica (){
	bool aux = consultarDatosEEPROM();
  if (aux == 0) {
    iniciarWifiTemporal();
    desconectarWifi();
    conectarWifi();
    enviarDatosEEPROM();
  }
  else {
    Serial.println("Los datos ssid y password se encuentran en la EEPROM: ");
    Serial.println(ssid);
    Serial.println(password);
    conectarWifi();
  }
}




