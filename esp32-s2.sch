EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 7
Title ""
Date "2021-05-27"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GNDD #PWR030
U 1 1 60B4618D
P 8700 3700
AR Path="/60B3AA65/60B4618D" Ref="#PWR030"  Part="1" 
AR Path="/62463654/60B4618D" Ref="#PWR?"  Part="1" 
F 0 "#PWR030" H 8700 3450 50  0001 C CNN
F 1 "GNDD" H 8704 3545 50  0000 C CNN
F 2 "" H 8700 3700 50  0001 C CNN
F 3 "" H 8700 3700 50  0001 C CNN
	1    8700 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal_GND24 Y2
U 1 1 62B20BAB
P 1250 3050
AR Path="/60B3AA65/62B20BAB" Ref="Y2"  Part="1" 
AR Path="/62463654/62B20BAB" Ref="Y?"  Part="1" 
F 0 "Y2" V 1100 2850 50  0000 L CNN
F 1 "40MHz" V 1450 2750 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_2016-4Pin_2.0x1.6mm" H 1250 3050 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/281/p79e-806516.pdf" H 1250 3050 50  0001 C CNN
F 4 "XRCGB40M000F4M02R0" H 1250 3050 50  0001 C CNN "Ref."
	1    1250 3050
	0    1    1    0   
$EndComp
Text Label 8300 2400 1    50   ~ 0
xtal+
Text Label 1250 3250 0    50   ~ 0
xtal-
Wire Wire Line
	8300 2400 8300 2700
Wire Wire Line
	8400 2700 8400 2400
$Comp
L power:GNDD #PWR032
U 1 1 60B05FBE
P 1700 3100
AR Path="/60B3AA65/60B05FBE" Ref="#PWR032"  Part="1" 
AR Path="/62463654/60B05FBE" Ref="#PWR?"  Part="1" 
F 0 "#PWR032" H 1700 2850 50  0001 C CNN
F 1 "GNDD" H 1704 2945 50  0000 C CNN
F 2 "" H 1700 3100 50  0001 C CNN
F 3 "" H 1700 3100 50  0001 C CNN
	1    1700 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 3050 1700 3100
$Comp
L power:GNDD #PWR031
U 1 1 60B06E18
P 800 3100
AR Path="/60B3AA65/60B06E18" Ref="#PWR031"  Part="1" 
AR Path="/62463654/60B06E18" Ref="#PWR?"  Part="1" 
F 0 "#PWR031" H 800 2850 50  0001 C CNN
F 1 "GNDD" H 804 2945 50  0000 C CNN
F 2 "" H 800 3100 50  0001 C CNN
F 3 "" H 800 3100 50  0001 C CNN
	1    800  3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  3050 800  3100
Text Label 900  2750 2    50   ~ 0
xtal+
Wire Wire Line
	1250 3200 1250 3250
Text Label 8400 2400 1    50   ~ 0
xtal-
Wire Wire Line
	8000 2400 8000 2700
Wire Wire Line
	8100 2400 8100 2700
Wire Wire Line
	8600 2400 8600 2700
Wire Wire Line
	8900 2400 8900 2700
Wire Wire Line
	9000 2400 9000 2700
Wire Wire Line
	9200 2400 9200 2700
Wire Wire Line
	9300 2400 9300 2700
Wire Wire Line
	9800 3400 10150 3400
Wire Wire Line
	9800 3300 10150 3300
Wire Wire Line
	9800 3100 10100 3100
Wire Wire Line
	9800 3500 10150 3500
Wire Wire Line
	7200 3400 7500 3400
Wire Wire Line
	7200 3300 7500 3300
Wire Wire Line
	7200 3200 7500 3200
Wire Wire Line
	7200 3800 7500 3800
Wire Wire Line
	7200 3700 7500 3700
Wire Wire Line
	7200 3600 7500 3600
Wire Wire Line
	7200 3500 7500 3500
Wire Wire Line
	7200 4200 7500 4200
Wire Wire Line
	7200 4100 7500 4100
Wire Wire Line
	7200 4000 7500 4000
Wire Wire Line
	7200 3900 7500 3900
Wire Wire Line
	7200 4400 7500 4400
Wire Wire Line
	7200 4300 7500 4300
Wire Wire Line
	8300 5200 8300 4900
Wire Wire Line
	8200 5200 8200 4900
Wire Wire Line
	8100 5200 8100 4900
Wire Wire Line
	8000 5200 8000 4900
Wire Wire Line
	8400 4900 8400 5200
Wire Wire Line
	8600 4900 8600 5200
Wire Wire Line
	8700 4900 8700 5200
Wire Wire Line
	8800 4900 8800 5200
Wire Wire Line
	8900 4900 8900 5200
Wire Wire Line
	9000 4900 9000 5200
Wire Wire Line
	9100 4900 9100 5200
Wire Wire Line
	9300 4900 9300 5200
Text GLabel 8000 2400 1    50   Input ~ 0
EN
Text GLabel 7200 3500 0    50   Input ~ 0
IO0
Text GLabel 8700 2400 1    50   Input ~ 0
RX0
Text GLabel 8800 2100 1    50   Input ~ 0
TX0
$Comp
L power:+3.3V #PWR?
U 1 1 60B54E2B
P 5600 3000
AR Path="/60B54E2B" Ref="#PWR?"  Part="1" 
AR Path="/60B3AA65/60B54E2B" Ref="#PWR035"  Part="1" 
AR Path="/62463654/60B54E2B" Ref="#PWR?"  Part="1" 
F 0 "#PWR035" H 5600 2850 50  0001 C CNN
F 1 "+3.3V" H 5615 3173 50  0000 C CNN
F 2 "" H 5600 3000 50  0001 C CNN
F 3 "" H 5600 3000 50  0001 C CNN
	1    5600 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3000 5600 3300
Wire Wire Line
	5600 3300 5900 3300
$Comp
L pspice:INDUCTOR L2
U 1 1 60B5781C
P 6500 3300
AR Path="/60B3AA65/60B5781C" Ref="L2"  Part="1" 
AR Path="/62463654/60B5781C" Ref="L?"  Part="1" 
F 0 "L2" H 6500 3515 50  0000 C CNN
F 1 "2nH" H 6500 3424 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 6500 3300 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/336/WC712-1772499.pdf" H 6500 3300 50  0001 C CNN
F 4 "Pulse Electronics" H 6500 3300 50  0001 C CNN "Manufacturer"
F 5 "PE-0402CL2N0STT" H 6500 3300 50  0001 C CNN "Ref."
	1    6500 3300
	1    0    0    -1  
$EndComp
Text GLabel 7200 3200 0    50   Input ~ 0
LNA_IN
Text GLabel 2300 6750 2    50   Input ~ 0
LNA_IN
$Comp
L Device:L L1
U 1 1 62B20BAD
P 1850 6750
AR Path="/60B3AA65/62B20BAD" Ref="L1"  Part="1" 
AR Path="/62463654/62B20BAD" Ref="L?"  Part="1" 
F 0 "L1" V 2040 6750 50  0000 C CNN
F 1 "2.7nH" V 1949 6750 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 1850 6750 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/AVX_LCCI-2000928.pdf" H 1850 6750 50  0001 C CNN
F 4 "avx" H 1850 6750 50  0001 C CNN "Manufacturer"
F 5 "LCCI0402S2N7GTAR" H 1850 6750 50  0001 C CNN "Ref."
	1    1850 6750
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 60B70244
P 2150 7000
AR Path="/60B70244" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60B70244" Ref="C11"  Part="1" 
AR Path="/62463654/60B70244" Ref="C?"  Part="1" 
F 0 "C11" H 2265 7046 50  0000 L CNN
F 1 "5.6pF" H 2265 6955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2188 6850 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/C0GNP0_Dielectric-951274.pdf" H 2150 7000 50  0001 C CNN
F 4 "04025A5R6BAT2A" H 2150 7000 50  0001 C CNN "Ref."
	1    2150 7000
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 60B706F3
P 1500 7050
AR Path="/60B706F3" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60B706F3" Ref="C10"  Part="1" 
AR Path="/62463654/60B706F3" Ref="C?"  Part="1" 
F 0 "C10" H 1615 7096 50  0000 L CNN
F 1 "2.4pF" H 1615 7005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1538 6900 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/281/GCM1555C1H2R4CA16_01-1964865.pdf" H 1500 7050 50  0001 C CNN
F 4 "GCM1555C1H2R4CA16D" H 1500 7050 50  0001 C CNN "Ref."
	1    1500 7050
	-1   0    0    1   
$EndComp
$Comp
L Device:Antenna_Shield AE1
U 1 1 60B71852
P 1100 6200
AR Path="/60B3AA65/60B71852" Ref="AE1"  Part="1" 
AR Path="/62463654/60B71852" Ref="AE?"  Part="1" 
F 0 "AE1" H 1020 6239 50  0000 R CNN
F 1 "Antenna_Shield" H 1020 6148 50  0000 R CNN
F 2 "_RF:3216_rf2-4ghz_antenna" H 1100 6300 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/products/datasheet/wireless/An_Ceramiz_2450_3216_L00_0.pdf" H 1100 6300 50  0001 C CNN
F 4 "ANT3216LL00R2400A" H 1100 6200 50  0001 C CNN "Ref."
	1    1100 6200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1100 6400 1100 6750
Wire Wire Line
	1100 6750 1500 6750
Connection ~ 1500 6750
Wire Wire Line
	1500 6750 1700 6750
Wire Wire Line
	2000 6750 2150 6750
Wire Wire Line
	2150 6850 2150 6750
Connection ~ 2150 6750
Wire Wire Line
	2150 6750 2300 6750
Wire Wire Line
	1500 6750 1500 6900
Wire Wire Line
	1000 6400 1000 7400
Wire Wire Line
	1500 7200 1500 7400
Wire Wire Line
	2150 7150 2150 7400
$Comp
L power:GNDD #PWR033
U 1 1 62B20BB1
P 1000 7400
AR Path="/60B3AA65/62B20BB1" Ref="#PWR033"  Part="1" 
AR Path="/62463654/62B20BB1" Ref="#PWR?"  Part="1" 
F 0 "#PWR033" H 1000 7150 50  0001 C CNN
F 1 "GNDD" H 1004 7245 50  0000 C CNN
F 2 "" H 1000 7400 50  0001 C CNN
F 3 "" H 1000 7400 50  0001 C CNN
	1    1000 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR034
U 1 1 62B20BB2
P 1500 7400
AR Path="/60B3AA65/62B20BB2" Ref="#PWR034"  Part="1" 
AR Path="/62463654/62B20BB2" Ref="#PWR?"  Part="1" 
F 0 "#PWR034" H 1500 7150 50  0001 C CNN
F 1 "GNDD" H 1504 7245 50  0000 C CNN
F 2 "" H 1500 7400 50  0001 C CNN
F 3 "" H 1500 7400 50  0001 C CNN
	1    1500 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR036
U 1 1 62B20BB3
P 2150 7400
AR Path="/60B3AA65/62B20BB3" Ref="#PWR036"  Part="1" 
AR Path="/62463654/62B20BB3" Ref="#PWR?"  Part="1" 
F 0 "#PWR036" H 2150 7150 50  0001 C CNN
F 1 "GNDD" H 2154 7245 50  0000 C CNN
F 2 "" H 2150 7400 50  0001 C CNN
F 3 "" H 2150 7400 50  0001 C CNN
	1    2150 7400
	1    0    0    -1  
$EndComp
Text Notes 2600 2700 0    50   ~ 0
Main Oscillator
Text Notes 2800 4400 0    50   ~ 0
RTC oscillator
Text Label 8600 5200 3    50   ~ 0
xtal_32+
Text Label 8700 5200 3    50   ~ 0
xtal_32-
$Comp
L pulsera_wifi-rescue:xtal_2-3NC_6914-_crystal Y3
U 1 1 60BE3CB6
P 1350 4575
AR Path="/60B3AA65/60BE3CB6" Ref="Y3"  Part="1" 
AR Path="/62463654/60BE3CB6" Ref="Y?"  Part="1" 
F 0 "Y3" H 1362 4733 50  0000 C CNN
F 1 "xtal_2-3NC_6914" H 1355 4710 50  0001 C CNN
F 2 "_xtal:xtal_1_P6.9x1.5mm" H 1450 4575 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1912111437_TXC-Corp-9H03200413_C337595.pdf" H 1350 4600 50  0001 C CNN
F 4 "9H03200413" H 1355 4910 50  0001 C CNN "Ref."
F 5 "TXC Corp" H 1355 5010 50  0001 C CNN "Manufacturer"
	1    1350 4575
	1    0    0    -1  
$EndComp
NoConn ~ 1575 4550
NoConn ~ 1575 4650
Text Label 625  4650 0    50   ~ 0
xtal_32+
Text Label 625  4550 0    50   ~ 0
xtal_32-
$Comp
L power:GNDD #PWR047
U 1 1 62B20BB4
P 2550 3200
AR Path="/60B3AA65/62B20BB4" Ref="#PWR047"  Part="1" 
AR Path="/62463654/62B20BB4" Ref="#PWR?"  Part="1" 
F 0 "#PWR047" H 2550 2950 50  0001 C CNN
F 1 "GNDD" H 2554 3045 50  0000 C CNN
F 2 "" H 2550 3200 50  0001 C CNN
F 3 "" H 2550 3200 50  0001 C CNN
	1    2550 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR048
U 1 1 62B20BB5
P 2150 3200
AR Path="/60B3AA65/62B20BB5" Ref="#PWR048"  Part="1" 
AR Path="/62463654/62B20BB5" Ref="#PWR?"  Part="1" 
F 0 "#PWR048" H 2150 2950 50  0001 C CNN
F 1 "GNDD" H 2154 3045 50  0000 C CNN
F 2 "" H 2150 3200 50  0001 C CNN
F 3 "" H 2150 3200 50  0001 C CNN
	1    2150 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 62B20BB6
P 2150 3050
AR Path="/62B20BB6" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BB6" Ref="C14"  Part="1" 
AR Path="/62463654/62B20BB6" Ref="C?"  Part="1" 
F 0 "C14" H 1950 2950 50  0000 L CNN
F 1 "DNP" H 1850 3100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2188 2900 50  0001 C CNN
F 3 "" H 2150 3050 50  0001 C CNN
F 4 "" H 2150 3050 50  0001 C CNN "Ref."
	1    2150 3050
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 62B20BB7
P 2550 3050
AR Path="/62B20BB7" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BB7" Ref="C13"  Part="1" 
AR Path="/62463654/62B20BB7" Ref="C?"  Part="1" 
F 0 "C13" H 2600 3150 50  0000 L CNN
F 1 "DNP" H 2665 3005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2588 2900 50  0001 C CNN
F 3 "" H 2550 3050 50  0001 C CNN
F 4 "" H 2550 3050 50  0001 C CNN "Ref."
	1    2550 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	625  4550 1150 4550
Wire Wire Line
	625  4650 1150 4650
Text Label 2300 4550 2    50   ~ 0
xtal_32-
Text Label 2700 4550 0    50   ~ 0
xtal_32+
$Comp
L power:GNDD #PWR050
U 1 1 60C93CEF
P 2700 4850
AR Path="/60B3AA65/60C93CEF" Ref="#PWR050"  Part="1" 
AR Path="/62463654/60C93CEF" Ref="#PWR?"  Part="1" 
F 0 "#PWR050" H 2700 4600 50  0001 C CNN
F 1 "GNDD" H 2704 4695 50  0000 C CNN
F 2 "" H 2700 4850 50  0001 C CNN
F 3 "" H 2700 4850 50  0001 C CNN
	1    2700 4850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR049
U 1 1 60C93CF5
P 2300 4850
AR Path="/60B3AA65/60C93CF5" Ref="#PWR049"  Part="1" 
AR Path="/62463654/60C93CF5" Ref="#PWR?"  Part="1" 
F 0 "#PWR049" H 2300 4600 50  0001 C CNN
F 1 "GNDD" H 2304 4695 50  0000 C CNN
F 2 "" H 2300 4850 50  0001 C CNN
F 3 "" H 2300 4850 50  0001 C CNN
	1    2300 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60C93CFC
P 2300 4700
AR Path="/60C93CFC" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60C93CFC" Ref="C15"  Part="1" 
AR Path="/62463654/60C93CFC" Ref="C?"  Part="1" 
F 0 "C15" H 2100 4600 50  0000 L CNN
F 1 "12pF" H 2000 4750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2338 4550 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/212/KEM_C1007_X8R_ULTRA_150C_SMD-1102703.pdf" H 2300 4700 50  0001 C CNN
F 4 "C0402C120J8HACTU" H 2300 4700 50  0001 C CNN "Ref."
	1    2300 4700
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 60C93D03
P 2700 4700
AR Path="/60C93D03" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60C93D03" Ref="C16"  Part="1" 
AR Path="/62463654/60C93D03" Ref="C?"  Part="1" 
F 0 "C16" H 2750 4800 50  0000 L CNN
F 1 "12pF" H 2815 4655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2738 4550 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/212/KEM_C1007_X8R_ULTRA_150C_SMD-1102703.pdf" H 2700 4700 50  0001 C CNN
F 4 "C0402C120J8HACTU" H 2700 4700 50  0001 C CNN "Ref."
	1    2700 4700
	1    0    0    -1  
$EndComp
Text Label 2550 2900 0    50   ~ 0
xtal-
$Comp
L power:+3.3V #PWR046
U 1 1 60CCD423
P 8500 1850
AR Path="/60B3AA65/60CCD423" Ref="#PWR046"  Part="1" 
AR Path="/62463654/60CCD423" Ref="#PWR?"  Part="1" 
F 0 "#PWR046" H 8500 1700 50  0001 C CNN
F 1 "+3.3V" H 8515 2023 50  0000 C CNN
F 2 "" H 8500 1850 50  0001 C CNN
F 3 "" H 8500 1850 50  0001 C CNN
	1    8500 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR045
U 1 1 60CFB5FF
P 7800 1850
AR Path="/60B3AA65/60CFB5FF" Ref="#PWR045"  Part="1" 
AR Path="/62463654/60CFB5FF" Ref="#PWR?"  Part="1" 
F 0 "#PWR045" H 7800 1600 50  0001 C CNN
F 1 "GNDD" H 7804 1695 50  0000 C CNN
F 2 "" H 7800 1850 50  0001 C CNN
F 3 "" H 7800 1850 50  0001 C CNN
	1    7800 1850
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 60D038AE
P 9200 5700
AR Path="/60D038AE" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60D038AE" Ref="C26"  Part="1" 
AR Path="/62463654/60D038AE" Ref="C?"  Part="1" 
F 0 "C26" H 9300 5700 50  0000 L CNN
F 1 "100nF" H 9250 5600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9238 5550 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 9200 5700 50  0001 C CNN
F 4 "0402YD104KAT2A" H 9200 5700 50  0001 C CNN "Ref."
	1    9200 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR053
U 1 1 60D0CF76
P 9200 5950
AR Path="/60B3AA65/60D0CF76" Ref="#PWR053"  Part="1" 
AR Path="/62463654/60D0CF76" Ref="#PWR?"  Part="1" 
F 0 "#PWR053" H 9200 5700 50  0001 C CNN
F 1 "GNDD" H 9204 5795 50  0000 C CNN
F 2 "" H 9200 5950 50  0001 C CNN
F 3 "" H 9200 5950 50  0001 C CNN
	1    9200 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60D1B874
P 6950 2800
AR Path="/60D1B874" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60D1B874" Ref="C21"  Part="1" 
AR Path="/62463654/60D1B874" Ref="C?"  Part="1" 
F 0 "C21" V 6800 2750 50  0000 L CNN
F 1 "100nF" V 6850 2500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 2650 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 6950 2800 50  0001 C CNN
F 4 "0402YD104KAT2A" H 6950 2800 50  0001 C CNN "Ref."
	1    6950 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR041
U 1 1 60D1B87A
P 6950 2650
AR Path="/60B3AA65/60D1B87A" Ref="#PWR041"  Part="1" 
AR Path="/62463654/60D1B87A" Ref="#PWR?"  Part="1" 
F 0 "#PWR041" H 6950 2400 50  0001 C CNN
F 1 "GNDD" H 6954 2495 50  0000 C CNN
F 2 "" H 6950 2650 50  0001 C CNN
F 3 "" H 6950 2650 50  0001 C CNN
	1    6950 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	6950 2950 6950 3100
$Comp
L power:+3.3V #PWR044
U 1 1 60D1E4B3
P 7200 2650
AR Path="/60B3AA65/60D1E4B3" Ref="#PWR044"  Part="1" 
AR Path="/62463654/60D1E4B3" Ref="#PWR?"  Part="1" 
F 0 "#PWR044" H 7200 2500 50  0001 C CNN
F 1 "+3.3V" H 7215 2823 50  0000 C CNN
F 2 "" H 7200 2650 50  0001 C CNN
F 3 "" H 7200 2650 50  0001 C CNN
	1    7200 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2650 7200 3100
Wire Wire Line
	7200 3100 7500 3100
Wire Wire Line
	7200 3300 7200 3400
Wire Wire Line
	6850 3300 6750 3300
$Comp
L Device:C C?
U 1 1 60D4C111
P 6200 3600
AR Path="/60D4C111" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60D4C111" Ref="C19"  Part="1" 
AR Path="/62463654/60D4C111" Ref="C?"  Part="1" 
F 0 "C19" H 6300 3600 50  0000 L CNN
F 1 "100nF" H 6250 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6238 3450 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 6200 3600 50  0001 C CNN
F 4 "0402YD104KAT2A" H 6200 3600 50  0001 C CNN "Ref."
	1    6200 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 62B20BBB
P 6850 3600
AR Path="/62B20BBB" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BBB" Ref="C20"  Part="1" 
AR Path="/62463654/62B20BBB" Ref="C?"  Part="1" 
F 0 "C20" H 6950 3600 50  0000 L CNN
F 1 "100nF" H 6900 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6888 3450 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 6850 3600 50  0001 C CNN
F 4 "0402YD104KAT2A" H 6850 3600 50  0001 C CNN "Ref."
	1    6850 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60D4DCC5
P 5900 3600
AR Path="/60D4DCC5" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60D4DCC5" Ref="C18"  Part="1" 
AR Path="/62463654/60D4DCC5" Ref="C?"  Part="1" 
F 0 "C18" H 5700 3600 50  0000 L CNN
F 1 "1uF" H 5700 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5938 3450 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/396/mlcc02_e-1307760.pdf" H 5900 3600 50  0001 C CNN
F 4 "EMK105BJ105KV-F" H 5900 3600 50  0001 C CNN "Ref."
	1    5900 3600
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR043
U 1 1 60D5BF6D
P 6850 3900
AR Path="/60B3AA65/60D5BF6D" Ref="#PWR043"  Part="1" 
AR Path="/62463654/60D5BF6D" Ref="#PWR?"  Part="1" 
F 0 "#PWR043" H 6850 3650 50  0001 C CNN
F 1 "GNDD" H 6854 3745 50  0000 C CNN
F 2 "" H 6850 3900 50  0001 C CNN
F 3 "" H 6850 3900 50  0001 C CNN
	1    6850 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR040
U 1 1 60D5C210
P 6200 3900
AR Path="/60B3AA65/60D5C210" Ref="#PWR040"  Part="1" 
AR Path="/62463654/60D5C210" Ref="#PWR?"  Part="1" 
F 0 "#PWR040" H 6200 3650 50  0001 C CNN
F 1 "GNDD" H 6204 3745 50  0000 C CNN
F 2 "" H 6200 3900 50  0001 C CNN
F 3 "" H 6200 3900 50  0001 C CNN
	1    6200 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR039
U 1 1 60D5C4EE
P 5900 3900
AR Path="/60B3AA65/60D5C4EE" Ref="#PWR039"  Part="1" 
AR Path="/62463654/60D5C4EE" Ref="#PWR?"  Part="1" 
F 0 "#PWR039" H 5900 3650 50  0001 C CNN
F 1 "GNDD" H 5904 3745 50  0000 C CNN
F 2 "" H 5900 3900 50  0001 C CNN
F 3 "" H 5900 3900 50  0001 C CNN
	1    5900 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR037
U 1 1 60D5C797
P 5600 3900
AR Path="/60B3AA65/60D5C797" Ref="#PWR037"  Part="1" 
AR Path="/62463654/60D5C797" Ref="#PWR?"  Part="1" 
F 0 "#PWR037" H 5600 3650 50  0001 C CNN
F 1 "GNDD" H 5604 3745 50  0000 C CNN
F 2 "" H 5600 3900 50  0001 C CNN
F 3 "" H 5600 3900 50  0001 C CNN
	1    5600 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60D5CE02
P 5600 3600
AR Path="/60D5CE02" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60D5CE02" Ref="C17"  Part="1" 
AR Path="/62463654/60D5CE02" Ref="C?"  Part="1" 
F 0 "C17" H 5400 3600 50  0000 L CNN
F 1 "10uF" H 5400 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5638 3450 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/281/1/GRM155R60J106ME15_01A-1983695.pdf" H 5600 3600 50  0001 C CNN
F 4 "GRM155R60J106ME15D" H 5600 3600 50  0001 C CNN "Ref."
	1    5600 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5600 3750 5600 3900
Wire Wire Line
	5600 3300 5600 3450
Connection ~ 5600 3300
Wire Wire Line
	5900 3300 5900 3450
Wire Wire Line
	5900 3750 5900 3900
Wire Wire Line
	6200 3300 6200 3450
Wire Wire Line
	6200 3750 6200 3900
Wire Wire Line
	6850 3300 6850 3450
Wire Wire Line
	6850 3750 6850 3900
$Comp
L Device:C C?
U 1 1 60D7CBAC
P 8500 5700
AR Path="/60D7CBAC" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60D7CBAC" Ref="C25"  Part="1" 
AR Path="/62463654/60D7CBAC" Ref="C?"  Part="1" 
F 0 "C25" H 8600 5700 50  0000 L CNN
F 1 "100nF" H 8550 5600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8538 5550 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 8500 5700 50  0001 C CNN
F 4 "0402YD104KAT2A" H 8500 5700 50  0001 C CNN "Ref."
	1    8500 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR051
U 1 1 60D7CBB2
P 8500 5950
AR Path="/60B3AA65/60D7CBB2" Ref="#PWR051"  Part="1" 
AR Path="/62463654/60D7CBB2" Ref="#PWR?"  Part="1" 
F 0 "#PWR051" H 8500 5700 50  0001 C CNN
F 1 "GNDD" H 8504 5795 50  0000 C CNN
F 2 "" H 8500 5950 50  0001 C CNN
F 3 "" H 8500 5950 50  0001 C CNN
	1    8500 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 5850 8500 5950
Wire Wire Line
	8500 4900 8500 5500
Connection ~ 8500 5500
Wire Wire Line
	8500 5500 8500 5550
$Comp
L power:+3.3V #PWR042
U 1 1 60D88EFB
P 8500 5500
AR Path="/60B3AA65/60D88EFB" Ref="#PWR042"  Part="1" 
AR Path="/62463654/60D88EFB" Ref="#PWR?"  Part="1" 
F 0 "#PWR042" H 8500 5350 50  0001 C CNN
F 1 "+3.3V" V 8500 5750 50  0000 C CNN
F 2 "" H 8500 5500 50  0001 C CNN
F 3 "" H 8500 5500 50  0001 C CNN
	1    8500 5500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 62B20BBF
P 10150 5000
AR Path="/62B20BBF" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BBF" Ref="C28"  Part="1" 
AR Path="/62463654/62B20BBF" Ref="C?"  Part="1" 
F 0 "C28" H 9950 5000 50  0000 L CNN
F 1 "1uF" H 9950 4900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10188 4850 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/396/mlcc02_e-1307760.pdf" H 10150 5000 50  0001 C CNN
F 4 "EMK105BJ105KV-F" H 10150 5000 50  0001 C CNN "Ref."
	1    10150 5000
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 62B20BC0
P 9950 5000
AR Path="/62B20BC0" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BC0" Ref="C27"  Part="1" 
AR Path="/62463654/62B20BC0" Ref="C?"  Part="1" 
F 0 "C27" H 10050 5000 50  0000 L CNN
F 1 "100nF" H 10050 4900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9988 4850 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 9950 5000 50  0001 C CNN
F 4 "0402YD104KAT2A" H 9950 5000 50  0001 C CNN "Ref."
	1    9950 5000
	-1   0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR057
U 1 1 60D92AC2
P 10150 5150
AR Path="/60B3AA65/60D92AC2" Ref="#PWR057"  Part="1" 
AR Path="/62463654/60D92AC2" Ref="#PWR?"  Part="1" 
F 0 "#PWR057" H 10150 4900 50  0001 C CNN
F 1 "GNDD" H 10154 4995 50  0000 C CNN
F 2 "" H 10150 5150 50  0001 C CNN
F 3 "" H 10150 5150 50  0001 C CNN
	1    10150 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR056
U 1 1 60DB772A
P 9950 5150
AR Path="/60B3AA65/60DB772A" Ref="#PWR056"  Part="1" 
AR Path="/62463654/60DB772A" Ref="#PWR?"  Part="1" 
F 0 "#PWR056" H 9950 4900 50  0001 C CNN
F 1 "GNDD" H 9954 4995 50  0000 C CNN
F 2 "" H 9950 5150 50  0001 C CNN
F 3 "" H 9950 5150 50  0001 C CNN
	1    9950 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10150 4850 10150 4750
Connection ~ 10150 4750
Wire Wire Line
	9950 4850 9950 4750
Text GLabel 10550 3700 2    50   Input ~ 0
SPID
Text GLabel 10550 3800 2    50   Input ~ 0
SPIQ
Text GLabel 10550 3900 2    50   Input ~ 0
SPICLK
Text GLabel 10550 4000 2    50   Input ~ 0
SPICS0
Text GLabel 10550 4100 2    50   Input ~ 0
SPIWP
Text GLabel 10550 4200 2    50   Input ~ 0
SPIHD
Text GLabel 10550 4400 2    50   Input ~ 0
SPICS1
Wire Wire Line
	9950 4750 10150 4750
$Comp
L power:+3.3V #PWR052
U 1 1 60E0F9FF
P 9100 1950
AR Path="/60B3AA65/60E0F9FF" Ref="#PWR052"  Part="1" 
AR Path="/62463654/60E0F9FF" Ref="#PWR?"  Part="1" 
F 0 "#PWR052" H 9100 1800 50  0001 C CNN
F 1 "+3.3V" H 9115 2123 50  0000 C CNN
F 2 "" H 9100 1950 50  0001 C CNN
F 3 "" H 9100 1950 50  0001 C CNN
	1    9100 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60E10123
P 9350 2100
AR Path="/60E10123" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60E10123" Ref="C24"  Part="1" 
AR Path="/62463654/60E10123" Ref="C?"  Part="1" 
F 0 "C24" V 9200 2050 50  0000 L CNN
F 1 "100nF" V 9250 1800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9388 1950 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 9350 2100 50  0001 C CNN
F 4 "0402YD104KAT2A" H 9350 2100 50  0001 C CNN "Ref."
	1    9350 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR054
U 1 1 60E1CF65
P 9350 1950
AR Path="/60B3AA65/60E1CF65" Ref="#PWR054"  Part="1" 
AR Path="/62463654/60E1CF65" Ref="#PWR?"  Part="1" 
F 0 "#PWR054" H 9350 1700 50  0001 C CNN
F 1 "GNDD" H 9354 1795 50  0000 C CNN
F 2 "" H 9350 1950 50  0001 C CNN
F 3 "" H 9350 1950 50  0001 C CNN
	1    9350 1950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R12
U 1 1 60E31530
P 8800 2250
AR Path="/60B3AA65/60E31530" Ref="R12"  Part="1" 
AR Path="/62463654/60E31530" Ref="R?"  Part="1" 
F 0 "R12" H 8650 2350 50  0000 L CNN
F 1 "499" V 8800 2200 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8730 2250 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 8800 2250 50  0001 C CNN
F 4 "Yageo" H 8800 2250 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-7D499RL" H 8800 2250 50  0001 C CNN "Ref."
	1    8800 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 2400 8800 2700
Wire Wire Line
	1200 2750 1250 2750
Wire Wire Line
	1250 2750 1250 2900
Wire Wire Line
	2150 2900 2150 2750
Wire Wire Line
	2150 2750 1250 2750
Connection ~ 1250 2750
$Comp
L Device:R R13
U 1 1 62B20BCF
P 1050 2750
AR Path="/60B3AA65/62B20BCF" Ref="R13"  Part="1" 
AR Path="/62463654/62B20BCF" Ref="R?"  Part="1" 
F 0 "R13" V 950 2800 50  0000 C CNN
F 1 "0" V 1050 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 980 2750 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 1050 2750 50  0001 C CNN
F 4 "Yageo" H 1050 2750 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-070RL" H 1050 2750 50  0001 C CNN "Ref."
	1    1050 2750
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 60E74866
P 1500 4900
AR Path="/60B3AA65/60E74866" Ref="R14"  Part="1" 
AR Path="/62463654/60E74866" Ref="R?"  Part="1" 
F 0 "R14" V 1400 4950 50  0000 C CNN
F 1 "5M" V 1500 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1430 4900 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 1500 4900 50  0001 C CNN
F 4 "Yageo" H 1500 4900 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-075M1L" H 1500 4900 50  0001 C CNN "Ref."
	1    1500 4900
	0    1    1    0   
$EndComp
Text Label 1350 4900 2    50   ~ 0
xtal_32+
Text Label 1650 4900 0    50   ~ 0
xtal_32-
$Comp
L Device:R_Pack04 RN1
U 1 1 62B20BAA
P 10350 3900
AR Path="/60B3AA65/62B20BAA" Ref="RN1"  Part="1" 
AR Path="/62463654/62B20BAA" Ref="RN?"  Part="1" 
F 0 "RN1" V 10200 3650 50  0000 C CNN
F 1 "R_Pack04" V 10300 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0402" V 10625 3900 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/315/AOC0000C14-1108062.pdf" H 10350 3900 50  0001 C CNN
F 4 "EXB-28VR000X" V 10350 3900 50  0001 C CNN "Ref."
F 5 "Panasonic" H 10350 3900 50  0001 C CNN "Manufacturer"
	1    10350 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	9800 3700 10150 3700
Wire Wire Line
	9800 3800 10150 3800
Wire Wire Line
	9800 3900 10150 3900
Wire Wire Line
	9800 4000 10150 4000
$Comp
L Device:R_Pack04 RN2
U 1 1 62B20BAC
P 10350 4300
AR Path="/60B3AA65/62B20BAC" Ref="RN2"  Part="1" 
AR Path="/62463654/62B20BAC" Ref="RN?"  Part="1" 
F 0 "RN2" V 10100 4050 50  0000 C CNN
F 1 "R_Pack04" V 10200 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0402" V 10625 4300 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/315/AOC0000C14-1108062.pdf" H 10350 4300 50  0001 C CNN
F 4 "EXB-28VR000X" V 10350 4300 50  0001 C CNN "Ref."
F 5 "Panasonic" H 10350 4300 50  0001 C CNN "Manufacturer"
	1    10350 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	9800 4200 10150 4200
Wire Wire Line
	9800 4100 10150 4100
$Comp
L _mcu:ESP32-S2 mcu1
U 1 1 60B42359
P 7500 3100
AR Path="/60B3AA65/60B42359" Ref="mcu1"  Part="1" 
AR Path="/62463654/60B42359" Ref="mcu?"  Part="1" 
F 0 "mcu1" H 8600 2800 50  0000 L CNN
F 1 "ESP32-S2" H 8500 2700 50  0000 L CNN
F 2 "Package_DFN_QFN:QFN-56-1EP_7x7mm_P0.4mm_EP5.6x5.6mm" H 7500 3100 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/891/esp32_s2_datasheet_en-1773066.pdf" H 7500 3100 50  0001 C CNN
F 4 "Espressif" H 7500 3100 50  0001 C CNN "Manufacturer"
F 5 "ESP32-S2" H 7500 3100 50  0001 C CNN "Ref."
	1    7500 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 4300 9950 4300
Wire Wire Line
	9950 4300 9950 4750
Connection ~ 9950 4750
Wire Wire Line
	9800 4400 10150 4400
NoConn ~ 10150 4300
NoConn ~ 10550 4300
NoConn ~ 7200 3900
NoConn ~ 7200 4000
NoConn ~ 7200 3800
NoConn ~ 7200 3600
NoConn ~ 7200 4100
NoConn ~ 7200 4200
Text GLabel 7200 4300 0    50   Input ~ 0
SDA
Text GLabel 7200 4400 0    50   Input ~ 0
SCL
Text GLabel 8000 5200 3    50   Input ~ 0
IT1
Text GLabel 8100 5200 3    50   Input ~ 0
IT2
Text GLabel 8200 5200 3    50   Input ~ 0
IT3
Text GLabel 8300 5200 3    50   Input ~ 0
IT4
$Comp
L Device:C C?
U 1 1 62B20BB8
P 1850 6400
AR Path="/62B20BB8" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BB8" Ref="C12"  Part="1" 
AR Path="/62463654/62B20BB8" Ref="C?"  Part="1" 
F 0 "C12" V 1900 6150 50  0000 L CNN
F 1 "DNP" V 1800 6150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1888 6250 50  0001 C CNN
F 3 "" H 1850 6400 50  0001 C CNN
F 4 "" H 1850 6400 50  0001 C CNN "Ref."
	1    1850 6400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1700 6400 1500 6400
Wire Wire Line
	1500 6400 1500 6750
Wire Wire Line
	2000 6400 2150 6400
Wire Wire Line
	2150 6400 2150 6750
Text GLabel 10550 3500 2    50   Input ~ 0
SS
Text GLabel 10550 3400 2    50   Input ~ 0
MOSI
Text GLabel 10550 3300 2    50   Input ~ 0
SCK
Text GLabel 10550 3200 2    50   Input ~ 0
MISO
$Comp
L Device:R_Pack04 RN3
U 1 1 60CE29D1
P 10350 3400
AR Path="/60B3AA65/60CE29D1" Ref="RN3"  Part="1" 
AR Path="/62463654/60CE29D1" Ref="RN?"  Part="1" 
F 0 "RN3" V 10200 3150 50  0000 C CNN
F 1 "R_Pack04" V 10300 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0402" V 10625 3400 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/315/AOC0000C14-1108062.pdf" H 10350 3400 50  0001 C CNN
F 4 "EXB-28VR000X" V 10350 3400 50  0001 C CNN "Ref."
F 5 "Panasonic" H 10350 3400 50  0001 C CNN "Manufacturer"
	1    10350 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	9800 3600 10100 3600
Wire Wire Line
	9800 3200 10150 3200
NoConn ~ 10100 3600
Text GLabel 8800 5200 3    50   Input ~ 0
TX1
Text GLabel 8900 5200 3    50   Input ~ 0
RX1
NoConn ~ 8400 5200
NoConn ~ 9000 5200
NoConn ~ 9100 5200
NoConn ~ 9300 5200
NoConn ~ 10100 3100
NoConn ~ 9300 2400
NoConn ~ 9200 2400
NoConn ~ 9000 2400
NoConn ~ 8900 2400
NoConn ~ 8600 2400
NoConn ~ 8100 2400
Text GLabel 5750 1300 2    50   Input ~ 0
IT1
Text GLabel 5750 1400 2    50   Input ~ 0
IT2
Text GLabel 5750 1500 2    50   Input ~ 0
IT3
Text GLabel 5750 1600 2    50   Input ~ 0
IT4
Text GLabel 5750 1100 2    50   Input ~ 0
SDA
Text GLabel 5750 1200 2    50   Input ~ 0
SCL
Text GLabel 1100 1200 2    50   Input ~ 0
TX1
Text GLabel 1100 1100 2    50   Input ~ 0
RX1
Text GLabel 2100 1200 2    50   Input ~ 0
RX0
Text GLabel 2100 1100 2    50   Input ~ 0
TX0
Text GLabel 4750 1400 2    50   Input ~ 0
SS
Text GLabel 4750 1300 2    50   Input ~ 0
MOSI
Text GLabel 4750 1200 2    50   Input ~ 0
SCK
Text GLabel 4750 1100 2    50   Input ~ 0
MISO
Text GLabel 3400 1050 2    50   Input ~ 0
SPID
Text GLabel 3400 1150 2    50   Input ~ 0
SPIQ
Text GLabel 3400 1250 2    50   Input ~ 0
SPICLK
Text GLabel 3400 1350 2    50   Input ~ 0
SPICS0
Text GLabel 3400 1450 2    50   Input ~ 0
SPIWP
Text GLabel 3400 1550 2    50   Input ~ 0
SPIHD
Text GLabel 3400 1650 2    50   Input ~ 0
SPICS1
$Comp
L power:+3.3V #PWR066
U 1 1 60DE4B36
P 3400 950
AR Path="/60B3AA65/60DE4B36" Ref="#PWR066"  Part="1" 
AR Path="/62463654/60DE4B36" Ref="#PWR?"  Part="1" 
F 0 "#PWR066" H 3400 800 50  0001 C CNN
F 1 "+3.3V" V 3400 1200 50  0000 C CNN
F 2 "" H 3400 950 50  0001 C CNN
F 3 "" H 3400 950 50  0001 C CNN
	1    3400 950 
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR067
U 1 1 62B20BC3
P 3400 1750
AR Path="/60B3AA65/62B20BC3" Ref="#PWR067"  Part="1" 
AR Path="/62463654/62B20BC3" Ref="#PWR?"  Part="1" 
F 0 "#PWR067" H 3400 1500 50  0001 C CNN
F 1 "GNDD" H 3404 1595 50  0000 C CNN
F 2 "" H 3400 1750 50  0001 C CNN
F 3 "" H 3400 1750 50  0001 C CNN
	1    3400 1750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR068
U 1 1 62B20BC4
P 4750 1000
AR Path="/60B3AA65/62B20BC4" Ref="#PWR068"  Part="1" 
AR Path="/62463654/62B20BC4" Ref="#PWR?"  Part="1" 
F 0 "#PWR068" H 4750 850 50  0001 C CNN
F 1 "+3.3V" V 4750 1250 50  0000 C CNN
F 2 "" H 4750 1000 50  0001 C CNN
F 3 "" H 4750 1000 50  0001 C CNN
	1    4750 1000
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR069
U 1 1 60DE975E
P 4750 1500
AR Path="/60B3AA65/60DE975E" Ref="#PWR069"  Part="1" 
AR Path="/62463654/60DE975E" Ref="#PWR?"  Part="1" 
F 0 "#PWR069" H 4750 1250 50  0001 C CNN
F 1 "GNDD" H 4754 1345 50  0000 C CNN
F 2 "" H 4750 1500 50  0001 C CNN
F 3 "" H 4750 1500 50  0001 C CNN
	1    4750 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR062
U 1 1 60DE9BF8
P 2100 1000
AR Path="/60B3AA65/60DE9BF8" Ref="#PWR062"  Part="1" 
AR Path="/62463654/60DE9BF8" Ref="#PWR?"  Part="1" 
F 0 "#PWR062" H 2100 850 50  0001 C CNN
F 1 "+3.3V" V 2100 1250 50  0000 C CNN
F 2 "" H 2100 1000 50  0001 C CNN
F 3 "" H 2100 1000 50  0001 C CNN
	1    2100 1000
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR064
U 1 1 60DEA463
P 1100 1000
AR Path="/60B3AA65/60DEA463" Ref="#PWR064"  Part="1" 
AR Path="/62463654/60DEA463" Ref="#PWR?"  Part="1" 
F 0 "#PWR064" H 1100 850 50  0001 C CNN
F 1 "+3.3V" V 1100 1250 50  0000 C CNN
F 2 "" H 1100 1000 50  0001 C CNN
F 3 "" H 1100 1000 50  0001 C CNN
	1    1100 1000
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR063
U 1 1 60DEA6EE
P 2100 1300
AR Path="/60B3AA65/60DEA6EE" Ref="#PWR063"  Part="1" 
AR Path="/62463654/60DEA6EE" Ref="#PWR?"  Part="1" 
F 0 "#PWR063" H 2100 1050 50  0001 C CNN
F 1 "GNDD" H 2104 1145 50  0000 C CNN
F 2 "" H 2100 1300 50  0001 C CNN
F 3 "" H 2100 1300 50  0001 C CNN
	1    2100 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR065
U 1 1 60DEAA1A
P 1100 1300
AR Path="/60B3AA65/60DEAA1A" Ref="#PWR065"  Part="1" 
AR Path="/62463654/60DEAA1A" Ref="#PWR?"  Part="1" 
F 0 "#PWR065" H 1100 1050 50  0001 C CNN
F 1 "GNDD" H 1104 1145 50  0000 C CNN
F 2 "" H 1100 1300 50  0001 C CNN
F 3 "" H 1100 1300 50  0001 C CNN
	1    1100 1300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR070
U 1 1 60DEAF5C
P 5750 1000
AR Path="/60B3AA65/60DEAF5C" Ref="#PWR070"  Part="1" 
AR Path="/62463654/60DEAF5C" Ref="#PWR?"  Part="1" 
F 0 "#PWR070" H 5750 850 50  0001 C CNN
F 1 "+3.3V" V 5750 1250 50  0000 C CNN
F 2 "" H 5750 1000 50  0001 C CNN
F 3 "" H 5750 1000 50  0001 C CNN
	1    5750 1000
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR071
U 1 1 62B20BC9
P 5750 1700
AR Path="/60B3AA65/62B20BC9" Ref="#PWR071"  Part="1" 
AR Path="/62463654/62B20BC9" Ref="#PWR?"  Part="1" 
F 0 "#PWR071" H 5750 1450 50  0001 C CNN
F 1 "GNDD" H 5754 1545 50  0000 C CNN
F 2 "" H 5750 1700 50  0001 C CNN
F 3 "" H 5750 1700 50  0001 C CNN
	1    5750 1700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 60DEBF4E
P 900 1100
AR Path="/60B3AA65/60DEBF4E" Ref="J5"  Part="1" 
AR Path="/62463654/60DEBF4E" Ref="J?"  Part="1" 
F 0 "J5" H 818 1417 50  0000 C CNN
F 1 "Serial_1" H 818 1326 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 900 1100 50  0001 C CNN
F 3 "~" H 900 1100 50  0001 C CNN
	1    900  1100
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 60DECEAB
P 1900 1100
AR Path="/60B3AA65/60DECEAB" Ref="J4"  Part="1" 
AR Path="/62463654/60DECEAB" Ref="J?"  Part="1" 
F 0 "J4" H 1818 1417 50  0000 C CNN
F 1 "Serial_0" H 1818 1326 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1900 1100 50  0001 C CNN
F 3 "~" H 1900 1100 50  0001 C CNN
	1    1900 1100
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J8
U 1 1 62B20BCB
P 5550 1300
AR Path="/60B3AA65/62B20BCB" Ref="J8"  Part="1" 
AR Path="/62463654/62B20BCB" Ref="J?"  Part="1" 
F 0 "J8" H 5468 1817 50  0000 C CNN
F 1 "I2C_IT" H 5468 1726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 5550 1300 50  0001 C CNN
F 3 "~" H 5550 1300 50  0001 C CNN
	1    5550 1300
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x09 J6
U 1 1 62B20BCC
P 3200 1350
AR Path="/60B3AA65/62B20BCC" Ref="J6"  Part="1" 
AR Path="/62463654/62B20BCC" Ref="J?"  Part="1" 
F 0 "J6" H 3118 1967 50  0000 C CNN
F 1 "SPI_Flash" H 3118 1876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Vertical" H 3200 1350 50  0001 C CNN
F 3 "~" H 3200 1350 50  0001 C CNN
	1    3200 1350
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J7
U 1 1 60DEFDFA
P 4550 1200
AR Path="/60B3AA65/60DEFDFA" Ref="J7"  Part="1" 
AR Path="/62463654/60DEFDFA" Ref="J?"  Part="1" 
F 0 "J7" H 4468 1617 50  0000 C CNN
F 1 "SPI" H 4468 1526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4550 1200 50  0001 C CNN
F 3 "~" H 4550 1200 50  0001 C CNN
	1    4550 1200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6950 3100 7200 3100
Connection ~ 7200 3100
Connection ~ 5900 3300
Wire Wire Line
	5900 3300 6200 3300
Connection ~ 6200 3300
Wire Wire Line
	6200 3300 6250 3300
Wire Wire Line
	6850 3300 7200 3300
Connection ~ 6850 3300
Connection ~ 7200 3300
Wire Wire Line
	9200 4900 9200 5450
$Comp
L power:+3.3V #PWR058
U 1 1 60F7276F
P 9200 5450
AR Path="/60B3AA65/60F7276F" Ref="#PWR058"  Part="1" 
AR Path="/62463654/60F7276F" Ref="#PWR?"  Part="1" 
F 0 "#PWR058" H 9200 5300 50  0001 C CNN
F 1 "+3.3V" V 9200 5700 50  0000 C CNN
F 2 "" H 9200 5450 50  0001 C CNN
F 3 "" H 9200 5450 50  0001 C CNN
	1    9200 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	9200 5850 9200 5950
Wire Wire Line
	9200 5450 9200 5550
Connection ~ 9200 5450
$Comp
L power:+3.3V #PWR061
U 1 1 60F8D209
P 10150 4750
AR Path="/60B3AA65/60F8D209" Ref="#PWR061"  Part="1" 
AR Path="/62463654/60F8D209" Ref="#PWR?"  Part="1" 
F 0 "#PWR061" H 10150 4600 50  0001 C CNN
F 1 "+3.3V" V 10150 5000 50  0000 C CNN
F 2 "" H 10150 4750 50  0001 C CNN
F 3 "" H 10150 4750 50  0001 C CNN
	1    10150 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	9100 1950 9100 2300
Wire Wire Line
	9350 2250 9350 2300
Wire Wire Line
	9350 2300 9100 2300
Connection ~ 9100 2300
Wire Wire Line
	9100 2300 9100 2700
$Comp
L Device:C C?
U 1 1 60CE02C2
P 8100 2000
AR Path="/60CE02C2" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60CE02C2" Ref="C23"  Part="1" 
AR Path="/62463654/60CE02C2" Ref="C?"  Part="1" 
F 0 "C23" V 7950 1950 50  0000 L CNN
F 1 "100nF" V 8000 1700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8138 1850 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 8100 2000 50  0001 C CNN
F 4 "0402YD104KAT2A" H 8100 2000 50  0001 C CNN "Ref."
	1    8100 2000
	1    0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 60CE02BB
P 7800 2000
AR Path="/60CE02BB" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60CE02BB" Ref="C22"  Part="1" 
AR Path="/62463654/60CE02BB" Ref="C?"  Part="1" 
F 0 "C22" V 7650 1950 50  0000 L CNN
F 1 "1uF" V 7700 1800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 1850 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/396/mlcc02_e-1307760.pdf" H 7800 2000 50  0001 C CNN
F 4 "EMK105BJ105KV-F" H 7800 2000 50  0001 C CNN "Ref."
	1    7800 2000
	1    0    0    1   
$EndComp
$Comp
L power:GNDD #PWR055
U 1 1 60FFD231
P 8100 1850
AR Path="/60B3AA65/60FFD231" Ref="#PWR055"  Part="1" 
AR Path="/62463654/60FFD231" Ref="#PWR?"  Part="1" 
F 0 "#PWR055" H 8100 1600 50  0001 C CNN
F 1 "GNDD" H 8104 1695 50  0000 C CNN
F 2 "" H 8100 1850 50  0001 C CNN
F 3 "" H 8100 1850 50  0001 C CNN
	1    8100 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	8700 2400 8700 2700
Wire Wire Line
	8200 2150 8100 2150
Wire Wire Line
	8200 2150 8200 2700
Connection ~ 8100 2150
Wire Wire Line
	8100 2150 7800 2150
Wire Wire Line
	8200 2150 8500 2150
Wire Wire Line
	8500 2150 8500 1850
Connection ~ 8200 2150
Wire Wire Line
	8500 2150 8500 2700
Connection ~ 8500 2150
Text Label 7200 3700 0    50   ~ 0
led_in
Text Label 5400 6600 0    50   ~ 0
led_in
$Comp
L Device:R R?
U 1 1 61251E14
P 5400 6750
AR Path="/61251E14" Ref="R?"  Part="1" 
AR Path="/60B3AA65/61251E14" Ref="R15"  Part="1" 
AR Path="/62463654/61251E14" Ref="R?"  Part="1" 
F 0 "R15" V 5300 6750 50  0000 C CNN
F 1 "1k" V 5500 6750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5330 6750 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 5400 6750 50  0001 C CNN
F 4 "Yageo" H 5400 6750 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-071KL" H 5400 6750 50  0001 C CNN "Ref."
	1    5400 6750
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 61251E1B
P 5400 7200
AR Path="/61251E1B" Ref="D?"  Part="1" 
AR Path="/60B3AA65/61251E1B" Ref="D5"  Part="1" 
AR Path="/62463654/61251E1B" Ref="D?"  Part="1" 
F 0 "D5" H 5450 7100 50  0000 R CNN
F 1 "Yellow" H 5500 7300 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 5400 7200 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/216/APG1005SYC-T-315808.pdf" H 5400 7200 50  0001 C CNN
F 4 "APG1005SYC-T" H 5400 7200 50  0001 C CNN "Ref."
	1    5400 7200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5400 6900 5400 7050
$Comp
L power:GNDD #PWR072
U 1 1 612561CD
P 5400 7350
AR Path="/60B3AA65/612561CD" Ref="#PWR072"  Part="1" 
AR Path="/62463654/612561CD" Ref="#PWR?"  Part="1" 
F 0 "#PWR072" H 5400 7100 50  0001 C CNN
F 1 "GNDD" H 5404 7195 50  0000 C CNN
F 2 "" H 5400 7350 50  0001 C CNN
F 3 "" H 5400 7350 50  0001 C CNN
	1    5400 7350
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 60C4F716
P 6550 3875
AR Path="/60B3AA65/60C4F716" Ref="#FLG01"  Part="1" 
AR Path="/62463654/60C4F716" Ref="#FLG?"  Part="1" 
F 0 "#FLG01" H 6550 3950 50  0001 C CNN
F 1 "PWR_FLAG" H 6550 4048 50  0000 C CNN
F 2 "" H 6550 3875 50  0001 C CNN
F 3 "~" H 6550 3875 50  0001 C CNN
	1    6550 3875
	-1   0    0    1   
$EndComp
Wire Wire Line
	6550 3875 6550 3450
Wire Wire Line
	6550 3450 6750 3450
Wire Wire Line
	6750 3450 6750 3300
Connection ~ 6750 3300
Wire Wire Line
	800  3050 1050 3050
Wire Wire Line
	1450 3050 1700 3050
$EndSCHEMATC
