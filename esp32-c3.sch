EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title "pulseWristBand"
Date "2022-03-26"
Rev "v004"
Comp "IKNX - ESWINO Corps"
Comment1 "mcu_sheet"
Comment2 "Designer: fmaw"
Comment3 "Reviewer: ace"
Comment4 "Manufacturer : IKNX "
$EndDescr
$Comp
L Device:Crystal_GND24 Y2
U 1 1 60B5D0AD
P 1150 2700
AR Path="/60B3AA65/60B5D0AD" Ref="Y2"  Part="1" 
AR Path="/62463654/60B5D0AD" Ref="Y4"  Part="1" 
F 0 "Y4" V 1000 2500 50  0000 L CNN
F 1 "40MHz" V 1350 2400 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_2016-4Pin_2.0x1.6mm" H 1150 2700 50  0001 C CNN
F 3 "https://www.murata.com/products/productdata/8801080115230/SPEC-XRCGB40M000F0L00R0.pdf?1541043011000" H 1150 2700 50  0001 C CNN
F 4 "XRCGB40M000F0L00R0" H 1150 2700 50  0001 C CNN "Ref."
	1    1150 2700
	0    1    1    0   
$EndComp
Text Label 1150 2900 0    50   ~ 0
xtal-
Text Label 800  2400 2    50   ~ 0
xtal+
Wire Wire Line
	1150 2850 1150 2900
Text GLabel 2650 4500 2    50   Input ~ 0
LNA_IN
$Comp
L Device:L L1
U 1 1 60B6F445
P 2200 4500
AR Path="/60B3AA65/60B6F445" Ref="L1"  Part="1" 
AR Path="/62463654/60B6F445" Ref="L4"  Part="1" 
F 0 "L4" V 2390 4500 50  0000 C CNN
F 1 "2.7nH" V 2299 4500 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 2200 4500 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/AVX_LCCI-2000928.pdf" H 2200 4500 50  0001 C CNN
F 4 "avx" H 2200 4500 50  0001 C CNN "Manufacturer"
F 5 "LCCI0402S2N7GTAR" H 2200 4500 50  0001 C CNN "Ref."
	1    2200 4500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 62B20BAE
P 2500 4750
AR Path="/62B20BAE" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BAE" Ref="C11"  Part="1" 
AR Path="/62463654/62B20BAE" Ref="C44"  Part="1" 
F 0 "C44" H 2615 4796 50  0000 L CNN
F 1 "5.6pF" H 2615 4705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2538 4600 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/C0GNP0_Dielectric-951274.pdf" H 2500 4750 50  0001 C CNN
F 4 "04025A5R6BAT2A" H 2500 4750 50  0001 C CNN "Ref."
	1    2500 4750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 62B20BAF
P 1850 4800
AR Path="/62B20BAF" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BAF" Ref="C10"  Part="1" 
AR Path="/62463654/62B20BAF" Ref="C42"  Part="1" 
F 0 "C42" H 1965 4846 50  0000 L CNN
F 1 "2.4pF" H 1965 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1888 4650 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/281/GCM1555C1H2R4CA16_01-1964865.pdf" H 1850 4800 50  0001 C CNN
F 4 "GCM1555C1H2R4CA16D" H 1850 4800 50  0001 C CNN "Ref."
	1    1850 4800
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 4150 1450 4500
Wire Wire Line
	1450 4500 1850 4500
Connection ~ 1850 4500
Wire Wire Line
	1850 4500 2050 4500
Wire Wire Line
	2350 4500 2500 4500
Wire Wire Line
	2500 4600 2500 4500
Connection ~ 2500 4500
Wire Wire Line
	2500 4500 2650 4500
Wire Wire Line
	1850 4500 1850 4650
Wire Wire Line
	1850 4950 1850 5150
Wire Wire Line
	2500 4900 2500 5150
$Comp
L power:GNDD #PWR034
U 1 1 60B811B5
P 1850 5150
AR Path="/60B3AA65/60B811B5" Ref="#PWR034"  Part="1" 
AR Path="/62463654/60B811B5" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 1850 4900 50  0001 C CNN
F 1 "GNDD" H 1854 4995 50  0000 C CNN
F 2 "" H 1850 5150 50  0001 C CNN
F 3 "" H 1850 5150 50  0001 C CNN
	1    1850 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR036
U 1 1 60B8150D
P 2500 5150
AR Path="/60B3AA65/60B8150D" Ref="#PWR036"  Part="1" 
AR Path="/62463654/60B8150D" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 2500 4900 50  0001 C CNN
F 1 "GNDD" H 2504 4995 50  0000 C CNN
F 2 "" H 2500 5150 50  0001 C CNN
F 3 "" H 2500 5150 50  0001 C CNN
	1    2500 5150
	1    0    0    -1  
$EndComp
Text Notes 1250 2200 0    50   ~ 0
Main Oscillator
$Comp
L power:GNDD #PWR047
U 1 1 60BFC7E7
P 2450 2850
AR Path="/60B3AA65/60BFC7E7" Ref="#PWR047"  Part="1" 
AR Path="/62463654/60BFC7E7" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 2450 2600 50  0001 C CNN
F 1 "GNDD" H 2454 2695 50  0000 C CNN
F 2 "" H 2450 2850 50  0001 C CNN
F 3 "" H 2450 2850 50  0001 C CNN
	1    2450 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR048
U 1 1 60BFCA43
P 2050 2850
AR Path="/60B3AA65/60BFCA43" Ref="#PWR048"  Part="1" 
AR Path="/62463654/60BFCA43" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 2050 2600 50  0001 C CNN
F 1 "GNDD" H 2054 2695 50  0000 C CNN
F 2 "" H 2050 2850 50  0001 C CNN
F 3 "" H 2050 2850 50  0001 C CNN
	1    2050 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60C09711
P 2050 2700
AR Path="/60C09711" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60C09711" Ref="C14"  Part="1" 
AR Path="/62463654/60C09711" Ref="C45"  Part="1" 
F 0 "C45" H 1850 2600 50  0000 L CNN
F 1 "6pF" H 1750 2750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2088 2550 50  0001 C CNN
F 3 "" H 2050 2700 50  0001 C CNN
F 4 "" H 2050 2700 50  0001 C CNN "Ref."
	1    2050 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 60C119DE
P 2450 2700
AR Path="/60C119DE" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60C119DE" Ref="C13"  Part="1" 
AR Path="/62463654/60C119DE" Ref="C46"  Part="1" 
F 0 "C46" H 2500 2800 50  0000 L CNN
F 1 "6pF" H 2565 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2488 2550 50  0001 C CNN
F 3 "" H 2450 2700 50  0001 C CNN
F 4 "" H 2450 2700 50  0001 C CNN "Ref."
	1    2450 2700
	1    0    0    -1  
$EndComp
Text Label 2450 2550 0    50   ~ 0
xtal-
$Comp
L Device:C C?
U 1 1 62B20BBA
P 10050 1400
AR Path="/62B20BBA" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BBA" Ref="C19"  Part="1" 
AR Path="/62463654/62B20BBA" Ref="C54"  Part="1" 
F 0 "C54" H 10100 1500 50  0000 L CNN
F 1 "100nF" H 10100 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10088 1250 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 10050 1400 50  0001 C CNN
F 4 "0402YD104KAT2A" H 10050 1400 50  0001 C CNN "Ref."
	1    10050 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 62B20BBC
P 7300 1400
AR Path="/62B20BBC" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BBC" Ref="C18"  Part="1" 
AR Path="/62463654/62B20BBC" Ref="C47"  Part="1" 
F 0 "C47" H 7350 1300 50  0000 L CNN
F 1 "1uF" H 7350 1500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7338 1250 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/396/mlcc02_e-1307760.pdf" H 7300 1400 50  0001 C CNN
F 4 "EMK105BJ105KV-F" H 7300 1400 50  0001 C CNN "Ref."
	1    7300 1400
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR039
U 1 1 62B20BBD
P 6650 1800
AR Path="/60B3AA65/62B20BBD" Ref="#PWR039"  Part="1" 
AR Path="/62463654/62B20BBD" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 6650 1550 50  0001 C CNN
F 1 "GNDD" H 6654 1645 50  0000 C CNN
F 2 "" H 6650 1800 50  0001 C CNN
F 3 "" H 6650 1800 50  0001 C CNN
	1    6650 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 62B20BBE
P 9800 1400
AR Path="/62B20BBE" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62B20BBE" Ref="C17"  Part="1" 
AR Path="/62463654/62B20BBE" Ref="C52"  Part="1" 
F 0 "C52" H 9650 1300 50  0000 L CNN
F 1 "10uF" H 9600 1500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9838 1250 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/281/1/GRM155R60J106ME15_01A-1983695.pdf" H 9800 1400 50  0001 C CNN
F 4 "GRM155R60J106ME15D" H 9800 1400 50  0001 C CNN "Ref."
	1    9800 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	9800 1550 9800 1700
Wire Wire Line
	9800 1100 9800 1250
Wire Wire Line
	7300 1100 7300 1250
Wire Wire Line
	7300 1550 7300 1700
Wire Wire Line
	10050 1100 10050 1250
Wire Wire Line
	10050 1550 10050 1700
$Comp
L Device:C C?
U 1 1 60D92AAC
P 8950 1400
AR Path="/60D92AAC" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60D92AAC" Ref="C28"  Part="1" 
AR Path="/62463654/60D92AAC" Ref="C50"  Part="1" 
F 0 "C50" H 8850 1500 50  0000 L CNN
F 1 "1uF" H 8800 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8988 1250 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/396/mlcc02_e-1307760.pdf" H 8950 1400 50  0001 C CNN
F 4 "EMK105BJ105KV-F" H 8950 1400 50  0001 C CNN "Ref."
	1    8950 1400
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60D92AB3
P 9200 1400
AR Path="/60D92AB3" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60D92AB3" Ref="C27"  Part="1" 
AR Path="/62463654/60D92AB3" Ref="C51"  Part="1" 
F 0 "C51" H 9100 1500 50  0000 L CNN
F 1 "10nF" H 8950 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9238 1250 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 9200 1400 50  0001 C CNN
F 4 "0402YD104KAT2A" H 9200 1400 50  0001 C CNN "Ref."
	1    9200 1400
	-1   0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR056
U 1 1 62B20BC1
P 9800 2000
AR Path="/60B3AA65/62B20BC1" Ref="#PWR056"  Part="1" 
AR Path="/62463654/62B20BC1" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 9800 1750 50  0001 C CNN
F 1 "GNDD" H 9804 1845 50  0000 C CNN
F 2 "" H 9800 2000 50  0001 C CNN
F 3 "" H 9800 2000 50  0001 C CNN
	1    9800 2000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8950 1250 8950 1100
Text GLabel 10500 3350 2    50   Input ~ 0
SPID
Text GLabel 10500 3250 2    50   Input ~ 0
SPIQ
Text GLabel 10500 3450 2    50   Input ~ 0
SPICLK
Text GLabel 10500 3550 2    50   Input ~ 0
SPICS0
Text GLabel 10500 3850 2    50   Input ~ 0
SPIWP
Text GLabel 10500 3950 2    50   Input ~ 0
SPIHD
$Comp
L Device:R R12
U 1 1 62B20BCE
P 6900 4000
AR Path="/60B3AA65/62B20BCE" Ref="R12"  Part="1" 
AR Path="/62463654/62B20BCE" Ref="R21"  Part="1" 
F 0 "R21" V 6950 4150 50  0000 L CNN
F 1 "499" V 6950 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6830 4000 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 6900 4000 50  0001 C CNN
F 4 "Yageo" H 6900 4000 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-7D499RL" H 6900 4000 50  0001 C CNN "Ref."
	1    6900 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1100 2400 1150 2400
Wire Wire Line
	1150 2400 1150 2550
Wire Wire Line
	2050 2550 2050 2400
Wire Wire Line
	2050 2400 1150 2400
Connection ~ 1150 2400
$Comp
L Device:R R13
U 1 1 60E531A3
P 950 2400
AR Path="/60B3AA65/60E531A3" Ref="R13"  Part="1" 
AR Path="/62463654/60E531A3" Ref="R18"  Part="1" 
F 0 "R18" V 850 2450 50  0000 C CNN
F 1 "0" V 950 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 880 2400 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 950 2400 50  0001 C CNN
F 4 "Yageo" H 950 2400 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-070RL" H 950 2400 50  0001 C CNN "Ref."
	1    950  2400
	0    1    1    0   
$EndComp
$Comp
L Device:R_Pack04 RN1
U 1 1 60B23F44
P 10300 3450
AR Path="/60B3AA65/60B23F44" Ref="RN1"  Part="1" 
AR Path="/62463654/60B23F44" Ref="RN5"  Part="1" 
F 0 "RN5" V 10000 3450 50  0000 C CNN
F 1 "R_Pack04" V 10500 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0402" V 10575 3450 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/315/AOC0000C14-1108062.pdf" H 10300 3450 50  0001 C CNN
F 4 "EXB-28VR000X" V 10300 3450 50  0001 C CNN "Ref."
F 5 "Panasonic" H 10300 3450 50  0001 C CNN "Manufacturer"
	1    10300 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Pack04 RN2
U 1 1 60B651B6
P 10300 4050
AR Path="/60B3AA65/60B651B6" Ref="RN2"  Part="1" 
AR Path="/62463654/60B651B6" Ref="RN6"  Part="1" 
F 0 "RN6" V 10000 4050 50  0000 C CNN
F 1 "R_Pack04" V 10500 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0402" V 10575 4050 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/315/AOC0000C14-1108062.pdf" H 10300 4050 50  0001 C CNN
F 4 "EXB-28VR000X" V 10300 4050 50  0001 C CNN "Ref."
F 5 "Panasonic" H 10300 4050 50  0001 C CNN "Manufacturer"
	1    10300 4050
	0    1    1    0   
$EndComp
NoConn ~ 10500 4050
Text GLabel 6600 3500 0    50   Input ~ 0
SDA
Text GLabel 6600 3600 0    50   Input ~ 0
SCL
Text GLabel 10500 4500 2    50   Input ~ 0
IT1
Text GLabel 10500 4600 2    50   Input ~ 0
IT2
Text GLabel 10500 4700 2    50   Input ~ 0
IT3
Text GLabel 10500 4800 2    50   Input ~ 0
IT4
$Comp
L Device:C C?
U 1 1 60CC0733
P 2200 4150
AR Path="/60CC0733" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60CC0733" Ref="C12"  Part="1" 
AR Path="/62463654/60CC0733" Ref="C43"  Part="1" 
F 0 "C43" V 2250 3900 50  0000 L CNN
F 1 "DNP" V 2150 3900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2238 4000 50  0001 C CNN
F 3 "" H 2200 4150 50  0001 C CNN
F 4 "" H 2200 4150 50  0001 C CNN "Ref."
	1    2200 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2050 4150 1850 4150
Wire Wire Line
	1850 4150 1850 4500
Wire Wire Line
	2350 4150 2500 4150
Wire Wire Line
	2500 4150 2500 4500
Text GLabel 6600 4350 0    50   Input ~ 0
SS
Text GLabel 6600 4450 0    50   Input ~ 0
MOSI
Text GLabel 6600 4650 0    50   Input ~ 0
SCK
Text GLabel 6600 4550 0    50   Input ~ 0
MISO
$Comp
L Device:R_Pack04 RN3
U 1 1 62B20BB9
P 6800 4550
AR Path="/60B3AA65/62B20BB9" Ref="RN3"  Part="1" 
AR Path="/62463654/62B20BB9" Ref="RN4"  Part="1" 
F 0 "RN4" V 7000 4550 50  0000 C CNN
F 1 "R_Pack04" V 6500 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0402" V 7075 4550 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/315/AOC0000C14-1108062.pdf" H 6800 4550 50  0001 C CNN
F 4 "EXB-28VR000X" V 6800 4550 50  0001 C CNN "Ref."
F 5 "Panasonic" H 6800 4550 50  0001 C CNN "Manufacturer"
	1    6800 4550
	0    -1   1    0   
$EndComp
Text GLabel 5750 1300 2    50   Input ~ 0
IT1
Text GLabel 5750 1400 2    50   Input ~ 0
IT2
Text GLabel 5750 1500 2    50   Input ~ 0
IT3
Text GLabel 5750 1600 2    50   Input ~ 0
IT4
Text GLabel 5750 1100 2    50   Input ~ 0
SDA
Text GLabel 5750 1200 2    50   Input ~ 0
SCL
Text GLabel 4750 1400 2    50   Input ~ 0
SS
Text GLabel 4750 1300 2    50   Input ~ 0
MOSI
Text GLabel 4750 1200 2    50   Input ~ 0
SCK
Text GLabel 4750 1100 2    50   Input ~ 0
MISO
Text GLabel 3400 1050 2    50   Input ~ 0
SPID
Text GLabel 3400 1150 2    50   Input ~ 0
SPIQ
Text GLabel 3400 1250 2    50   Input ~ 0
SPICLK
Text GLabel 3400 1350 2    50   Input ~ 0
SPICS0
Text GLabel 3400 1450 2    50   Input ~ 0
SPIWP
Text GLabel 3400 1550 2    50   Input ~ 0
SPIHD
Text GLabel 3400 1650 2    50   Input ~ 0
SPICS1
$Comp
L power:+3.3V #PWR066
U 1 1 62B20BC2
P 3400 950
AR Path="/60B3AA65/62B20BC2" Ref="#PWR066"  Part="1" 
AR Path="/62463654/62B20BC2" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 3400 800 50  0001 C CNN
F 1 "+3.3V" V 3400 1200 50  0000 C CNN
F 2 "" H 3400 950 50  0001 C CNN
F 3 "" H 3400 950 50  0001 C CNN
	1    3400 950 
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR067
U 1 1 60DE8B43
P 3400 1750
AR Path="/60B3AA65/60DE8B43" Ref="#PWR067"  Part="1" 
AR Path="/62463654/60DE8B43" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 3400 1500 50  0001 C CNN
F 1 "GNDD" H 3404 1595 50  0000 C CNN
F 2 "" H 3400 1750 50  0001 C CNN
F 3 "" H 3400 1750 50  0001 C CNN
	1    3400 1750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR068
U 1 1 60DE932F
P 4750 1000
AR Path="/60B3AA65/60DE932F" Ref="#PWR068"  Part="1" 
AR Path="/62463654/60DE932F" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 4750 850 50  0001 C CNN
F 1 "+3.3V" V 4750 1250 50  0000 C CNN
F 2 "" H 4750 1000 50  0001 C CNN
F 3 "" H 4750 1000 50  0001 C CNN
	1    4750 1000
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR069
U 1 1 62B20BC5
P 4750 1500
AR Path="/60B3AA65/62B20BC5" Ref="#PWR069"  Part="1" 
AR Path="/62463654/62B20BC5" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 4750 1250 50  0001 C CNN
F 1 "GNDD" H 4754 1345 50  0000 C CNN
F 2 "" H 4750 1500 50  0001 C CNN
F 3 "" H 4750 1500 50  0001 C CNN
	1    4750 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR070
U 1 1 62B20BC8
P 5750 1000
AR Path="/60B3AA65/62B20BC8" Ref="#PWR070"  Part="1" 
AR Path="/62463654/62B20BC8" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 5750 850 50  0001 C CNN
F 1 "+3.3V" V 5750 1250 50  0000 C CNN
F 2 "" H 5750 1000 50  0001 C CNN
F 3 "" H 5750 1000 50  0001 C CNN
	1    5750 1000
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR071
U 1 1 60DEB3AC
P 5750 1700
AR Path="/60B3AA65/60DEB3AC" Ref="#PWR071"  Part="1" 
AR Path="/62463654/60DEB3AC" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 5750 1450 50  0001 C CNN
F 1 "GNDD" H 5754 1545 50  0000 C CNN
F 2 "" H 5750 1700 50  0001 C CNN
F 3 "" H 5750 1700 50  0001 C CNN
	1    5750 1700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J8
U 1 1 60DEDE9F
P 5550 1300
AR Path="/60B3AA65/60DEDE9F" Ref="J8"  Part="1" 
AR Path="/62463654/60DEDE9F" Ref="J12"  Part="1" 
F 0 "J12" H 5468 1817 50  0000 C CNN
F 1 "I2C_IT" H 5468 1726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 5550 1300 50  0001 C CNN
F 3 "~" H 5550 1300 50  0001 C CNN
	1    5550 1300
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x09 J6
U 1 1 60DEF1EA
P 3200 1350
AR Path="/60B3AA65/60DEF1EA" Ref="J6"  Part="1" 
AR Path="/62463654/60DEF1EA" Ref="J10"  Part="1" 
F 0 "J10" H 3118 1967 50  0000 C CNN
F 1 "SPI_Flash" H 3118 1876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Vertical" H 3200 1350 50  0001 C CNN
F 3 "~" H 3200 1350 50  0001 C CNN
	1    3200 1350
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J7
U 1 1 62B20BCD
P 4550 1200
AR Path="/60B3AA65/62B20BCD" Ref="J7"  Part="1" 
AR Path="/62463654/62B20BCD" Ref="J11"  Part="1" 
F 0 "J11" H 4468 1617 50  0000 C CNN
F 1 "SPI" H 4468 1526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4550 1200 50  0001 C CNN
F 3 "~" H 4550 1200 50  0001 C CNN
	1    4550 1200
	-1   0    0    -1  
$EndComp
Text Label 1000 6350 0    50   ~ 0
led_in
$Comp
L Device:R R?
U 1 1 62B20BD0
P 1000 6500
AR Path="/62B20BD0" Ref="R?"  Part="1" 
AR Path="/60B3AA65/62B20BD0" Ref="R15"  Part="1" 
AR Path="/62463654/62B20BD0" Ref="R20"  Part="1" 
F 0 "R20" V 900 6500 50  0000 C CNN
F 1 "1k" V 1100 6500 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 930 6500 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 1000 6500 50  0001 C CNN
F 4 "Yageo" H 1000 6500 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-071KL" H 1000 6500 50  0001 C CNN "Ref."
	1    1000 6500
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 62B20BD1
P 1000 6950
AR Path="/62B20BD1" Ref="D?"  Part="1" 
AR Path="/60B3AA65/62B20BD1" Ref="D5"  Part="1" 
AR Path="/62463654/62B20BD1" Ref="D6"  Part="1" 
F 0 "D6" H 1050 6850 50  0000 R CNN
F 1 "Yellow" H 1100 7050 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 1000 6950 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/216/APG1005SYC-T-315808.pdf" H 1000 6950 50  0001 C CNN
F 4 "APG1005SYC-T" H 1000 6950 50  0001 C CNN "Ref."
	1    1000 6950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1000 6650 1000 6800
$Comp
L power:GNDD #PWR072
U 1 1 62B20BD2
P 1000 7100
AR Path="/60B3AA65/62B20BD2" Ref="#PWR072"  Part="1" 
AR Path="/62463654/62B20BD2" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 1000 6850 50  0001 C CNN
F 1 "GNDD" H 1004 6945 50  0000 C CNN
F 2 "" H 1000 7100 50  0001 C CNN
F 3 "" H 1000 7100 50  0001 C CNN
	1    1000 7100
	1    0    0    -1  
$EndComp
Text Label 7750 2900 2    50   ~ 0
VDDA
Text Label 7750 2800 2    50   ~ 0
VDD3P3
Text Label 7750 2700 2    50   ~ 0
VDD3P3_RTC
Text Label 7750 2500 2    50   ~ 0
VDD_SPI
Text Label 7750 2600 2    50   ~ 0
VDD3P3_CPU
Wire Wire Line
	7750 2900 7950 2900
Wire Wire Line
	7950 2900 7950 3000
Wire Wire Line
	8050 3000 8050 2800
Wire Wire Line
	8050 2800 7750 2800
Wire Wire Line
	7750 2700 8150 2700
Wire Wire Line
	8150 2700 8150 3000
Wire Wire Line
	8250 3000 8250 2600
Wire Wire Line
	8250 2600 7750 2600
Wire Wire Line
	7750 2500 8350 2500
Wire Wire Line
	8350 2500 8350 3000
Text Label 7300 3250 2    50   ~ 0
xtal-
Text Label 7300 3350 2    50   ~ 0
xtal+
Text GLabel 6600 4100 0    50   Input ~ 0
EN
Text GLabel 6600 3900 0    50   Input ~ 0
RX0
Text GLabel 6600 4000 0    50   Input ~ 0
TX0
$Comp
L power:GNDD #PWR?
U 1 1 6258F6EB
P 8150 5050
AR Path="/60B3AA65/6258F6EB" Ref="#PWR?"  Part="1" 
AR Path="/62463654/6258F6EB" Ref="#PWR0122"  Part="1" 
F 0 "#PWR0122" H 8150 4800 50  0001 C CNN
F 1 "GNDD" H 8154 4895 50  0000 C CNN
F 2 "" H 8150 5050 50  0001 C CNN
F 3 "" H 8150 5050 50  0001 C CNN
	1    8150 5050
	1    0    0    -1  
$EndComp
Text GLabel 9400 3250 2    50   Input ~ 0
LNA_IN
Text Label 8950 950  2    50   ~ 0
VDDA
Text Label 10750 950  0    50   ~ 0
VDD3P3
Text Label 8550 950  2    50   ~ 0
VDD3P3_RTC
Text Label 7300 950  2    50   ~ 0
VDD_SPI
Text Label 7950 950  2    50   ~ 0
VDD3P3_CPU
Wire Wire Line
	6750 4000 6600 4000
Wire Wire Line
	9400 3250 9000 3250
Text Label 9000 4300 0    50   ~ 0
led_in
NoConn ~ 10500 4150
NoConn ~ 10100 4150
NoConn ~ 10100 4050
Wire Wire Line
	9000 3400 9800 3400
Wire Wire Line
	9800 3400 9800 3250
Wire Wire Line
	9800 3250 10100 3250
Wire Wire Line
	10100 3350 9900 3350
Wire Wire Line
	9900 3500 9000 3500
Wire Wire Line
	9900 3350 9900 3500
Wire Wire Line
	9000 3600 10000 3600
Wire Wire Line
	10000 3600 10000 3450
Wire Wire Line
	10000 3450 10100 3450
Wire Wire Line
	9000 3700 10100 3700
Wire Wire Line
	10100 3700 10100 3550
Wire Wire Line
	9000 3800 10100 3800
Wire Wire Line
	10100 3800 10100 3850
Wire Wire Line
	10100 3950 10000 3950
Wire Wire Line
	10000 3950 10000 3900
Wire Wire Line
	10000 3900 9000 3900
$Comp
L power:+3.3V #PWR?
U 1 1 6270690B
P 6650 1000
AR Path="/6270690B" Ref="#PWR?"  Part="1" 
AR Path="/60B3AA65/6270690B" Ref="#PWR?"  Part="1" 
AR Path="/62463654/6270690B" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0120" H 6650 850 50  0001 C CNN
F 1 "+3.3V" H 6665 1173 50  0000 C CNN
F 2 "" H 6650 1000 50  0001 C CNN
F 3 "" H 6650 1000 50  0001 C CNN
	1    6650 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 1000 6650 1100
Wire Wire Line
	6650 1100 7300 1100
Wire Wire Line
	8950 950  8950 1100
Wire Wire Line
	8550 950  8550 1100
Connection ~ 8550 1100
Wire Wire Line
	8550 1100 8950 1100
Wire Wire Line
	7950 950  7950 1100
Connection ~ 7950 1100
Wire Wire Line
	7950 1100 8550 1100
Wire Wire Line
	7300 950  7300 1100
Connection ~ 7300 1100
Wire Wire Line
	7300 1100 7950 1100
$Comp
L pspice:INDUCTOR L?
U 1 1 6274C265
P 10350 1100
AR Path="/60B3AA65/6274C265" Ref="L?"  Part="1" 
AR Path="/62463654/6274C265" Ref="L5"  Part="1" 
F 0 "L5" H 10650 1150 50  0000 C CNN
F 1 "2nH" H 10050 1150 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 10350 1100 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/336/WC712-1772499.pdf" H 10350 1100 50  0001 C CNN
F 4 "Pulse Electronics" H 10350 1100 50  0001 C CNN "Manufacturer"
F 5 "PE-0402CL2N0STT" H 10350 1100 50  0001 C CNN "Ref."
	1    10350 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 1100 10600 1100
$Comp
L Device:C C?
U 1 1 627934C8
P 7950 1400
AR Path="/627934C8" Ref="C?"  Part="1" 
AR Path="/60B3AA65/627934C8" Ref="C?"  Part="1" 
AR Path="/62463654/627934C8" Ref="C48"  Part="1" 
F 0 "C48" H 8000 1500 50  0000 L CNN
F 1 "100nF" H 8000 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7988 1250 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 7950 1400 50  0001 C CNN
F 4 "0402YD104KAT2A" H 7950 1400 50  0001 C CNN "Ref."
	1    7950 1400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7950 1100 7950 1250
Wire Wire Line
	7950 1550 7950 1700
$Comp
L Device:C C?
U 1 1 627FEBAB
P 8550 1400
AR Path="/627FEBAB" Ref="C?"  Part="1" 
AR Path="/60B3AA65/627FEBAB" Ref="C?"  Part="1" 
AR Path="/62463654/627FEBAB" Ref="C49"  Part="1" 
F 0 "C49" H 8600 1500 50  0000 L CNN
F 1 "100nF" H 8600 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8588 1250 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 8550 1400 50  0001 C CNN
F 4 "0402YD104KAT2A" H 8550 1400 50  0001 C CNN "Ref."
	1    8550 1400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8550 1100 8550 1250
Wire Wire Line
	8550 1550 8550 1700
$Comp
L power:+3.3V #PWR?
U 1 1 62837C67
P 9800 750
AR Path="/62837C67" Ref="#PWR?"  Part="1" 
AR Path="/60B3AA65/62837C67" Ref="#PWR?"  Part="1" 
AR Path="/62463654/62837C67" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 9800 600 50  0001 C CNN
F 1 "+3.3V" H 9815 923 50  0000 C CNN
F 2 "" H 9800 750 50  0001 C CNN
F 3 "" H 9800 750 50  0001 C CNN
	1    9800 750 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10100 1100 10050 1100
Wire Wire Line
	9800 1100 9800 750 
Connection ~ 8950 1100
Wire Wire Line
	6650 1800 6650 1700
Wire Wire Line
	6650 1700 7300 1700
Wire Wire Line
	9200 1100 9200 1250
Wire Wire Line
	8950 1100 9200 1100
Wire Wire Line
	7300 1700 7950 1700
Wire Wire Line
	9200 1700 9200 1550
Connection ~ 7300 1700
Connection ~ 7950 1700
Wire Wire Line
	7950 1700 8550 1700
Connection ~ 8550 1700
Wire Wire Line
	8550 1700 8950 1700
Wire Wire Line
	8950 1550 8950 1700
Connection ~ 8950 1700
Wire Wire Line
	8950 1700 9200 1700
Wire Wire Line
	9800 2000 9800 1700
Wire Wire Line
	9800 1700 10050 1700
Text GLabel 10500 4900 2    50   Input ~ 0
D-
Text GLabel 10500 5000 2    50   Input ~ 0
D+
$Comp
L Device:C C?
U 1 1 62936CBB
P 10150 5550
AR Path="/62936CBB" Ref="C?"  Part="1" 
AR Path="/60B3AA65/62936CBB" Ref="C?"  Part="1" 
AR Path="/62463654/62936CBB" Ref="C55"  Part="1" 
F 0 "C55" H 10000 5450 50  0000 L CNN
F 1 "5.6pF" H 9900 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10188 5400 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/C0GNP0_Dielectric-951274.pdf" H 10150 5550 50  0001 C CNN
F 4 "04025A5R6BAT2A" H 10150 5550 50  0001 C CNN "Ref."
	1    10150 5550
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 6293760A
P 9850 5550
AR Path="/6293760A" Ref="C?"  Part="1" 
AR Path="/60B3AA65/6293760A" Ref="C?"  Part="1" 
AR Path="/62463654/6293760A" Ref="C53"  Part="1" 
F 0 "C53" H 9700 5450 50  0000 L CNN
F 1 "5.6pF" H 9600 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9888 5400 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/C0GNP0_Dielectric-951274.pdf" H 9850 5550 50  0001 C CNN
F 4 "04025A5R6BAT2A" H 9850 5550 50  0001 C CNN "Ref."
	1    9850 5550
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 6293E291
P 9850 5950
AR Path="/60B3AA65/6293E291" Ref="#PWR?"  Part="1" 
AR Path="/62463654/6293E291" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 9850 5700 50  0001 C CNN
F 1 "GNDD" H 9854 5795 50  0000 C CNN
F 2 "" H 9850 5950 50  0001 C CNN
F 3 "" H 9850 5950 50  0001 C CNN
	1    9850 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 5700 9850 5850
Wire Wire Line
	9850 5850 10150 5850
Wire Wire Line
	10150 5850 10150 5700
Connection ~ 9850 5850
Wire Wire Line
	9850 5850 9850 5950
Wire Wire Line
	6600 3500 7300 3500
Wire Wire Line
	6600 3600 7300 3600
Wire Wire Line
	6600 3900 7300 3900
Wire Wire Line
	7050 4000 7300 4000
Wire Wire Line
	6600 4100 7300 4100
Wire Wire Line
	7000 4350 7300 4350
Wire Wire Line
	7000 4450 7300 4450
Wire Wire Line
	7000 4550 7300 4550
Wire Wire Line
	7000 4650 7300 4650
Wire Wire Line
	8150 4900 8150 5050
$Comp
L _mcu:ESP32-C3 mcu2
U 1 1 624A160A
P 8150 4000
F 0 "mcu2" H 9050 5100 50  0000 C CNN
F 1 "ESP32-C3" H 9150 5000 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-32-1EP_5x5mm_P0.5mm_EP3.1x3.1mm_ThermalVias" H 8900 1400 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-c3_datasheet_en.pdf" H 7050 1450 50  0001 C CNN
	1    8150 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 4100 9900 4100
Wire Wire Line
	9900 4100 9900 4500
Wire Wire Line
	9900 4500 10500 4500
Wire Wire Line
	9800 4600 9800 4200
Wire Wire Line
	9800 4200 9000 4200
Wire Wire Line
	9800 4600 10500 4600
Wire Wire Line
	9000 4400 9700 4400
Wire Wire Line
	9700 4400 9700 4700
Wire Wire Line
	9700 4700 10500 4700
Wire Wire Line
	9600 4800 9600 4500
Wire Wire Line
	9600 4500 9000 4500
Wire Wire Line
	9600 4800 10500 4800
Wire Wire Line
	9000 4600 9500 4600
Wire Wire Line
	9500 4600 9500 4900
Wire Wire Line
	9400 5000 9400 4700
Wire Wire Line
	9400 4700 9000 4700
Wire Wire Line
	9850 4900 9850 5400
Wire Wire Line
	9500 4900 9850 4900
Connection ~ 10150 5000
Wire Wire Line
	10150 5000 9400 5000
Wire Wire Line
	10150 5000 10150 5400
Connection ~ 9800 1100
Connection ~ 9800 1700
Connection ~ 10050 1100
Wire Wire Line
	10050 1100 9800 1100
Connection ~ 10050 1700
Wire Wire Line
	10050 1700 10750 1700
Wire Wire Line
	10750 950  10750 1100
Wire Wire Line
	10750 1550 10750 1700
Wire Wire Line
	10750 1100 10750 1250
$Comp
L Device:C C?
U 1 1 60D4D32A
P 10750 1400
AR Path="/60D4D32A" Ref="C?"  Part="1" 
AR Path="/60B3AA65/60D4D32A" Ref="C20"  Part="1" 
AR Path="/62463654/60D4D32A" Ref="C56"  Part="1" 
F 0 "C56" H 10850 1400 50  0000 L CNN
F 1 "100nF" H 10800 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10788 1250 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/40/cx5r-776519.pdf" H 10750 1400 50  0001 C CNN
F 4 "0402YD104KAT2A" H 10750 1400 50  0001 C CNN "Ref."
	1    10750 1400
	1    0    0    -1  
$EndComp
Connection ~ 10750 1100
$Comp
L Device:R R?
U 1 1 62AAC8EC
P 10300 4900
AR Path="/62AAC8EC" Ref="R?"  Part="1" 
AR Path="/62463654/62AAC8EC" Ref="R22"  Part="1" 
F 0 "R22" V 10350 5100 50  0000 C CNN
F 1 "0" V 10350 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 10230 4900 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 10300 4900 50  0001 C CNN
F 4 "RC0402FR-070RL" V 10300 4900 50  0001 C CNN "Ref."
F 5 "Yageo" H 10300 4900 50  0001 C CNN "Manufacturer"
	1    10300 4900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62AB0B2D
P 10300 5000
AR Path="/62AB0B2D" Ref="R?"  Part="1" 
AR Path="/62463654/62AB0B2D" Ref="R23"  Part="1" 
F 0 "R23" V 10350 5200 50  0000 C CNN
F 1 "0" V 10350 4850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 10230 5000 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 10300 5000 50  0001 C CNN
F 4 "RC0402FR-070RL" V 10300 5000 50  0001 C CNN "Ref."
F 5 "Yageo" H 10300 5000 50  0001 C CNN "Manufacturer"
	1    10300 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10500 4900 10450 4900
Wire Wire Line
	10500 5000 10450 5000
Wire Wire Line
	10150 4900 9850 4900
Connection ~ 9850 4900
Wire Notes Line
	8700 800  8700 1850
Wire Notes Line
	9450 1850 9450 800 
Wire Notes Line
	6900 800  6900 1850
Wire Notes Line
	8050 800  8050 1850
Wire Notes Line
	7450 800  7450 1850
Wire Notes Line
	11100 800  11100 1850
Wire Notes Line
	6900 800  11100 800 
Wire Notes Line
	6900 1850 11100 1850
NoConn ~ 950  2700
NoConn ~ 1350 2700
Text Label 4250 6450 1    50   ~ 0
VDD3P3
Wire Wire Line
	4250 6700 4250 6450
$Comp
L power:Earth #PWR034
U 1 1 6243909A
P 1250 5200
F 0 "#PWR034" H 1250 4950 50  0001 C CNN
F 1 "Earth" H 1250 5050 50  0001 C CNN
F 2 "" H 1250 5200 50  0001 C CNN
F 3 "~" H 1250 5200 50  0001 C CNN
	1    1250 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 4150 1250 5200
$Comp
L power:Earth #PWR035
U 1 1 6243F38B
P 4650 6500
F 0 "#PWR035" H 4650 6250 50  0001 C CNN
F 1 "Earth" H 4650 6350 50  0001 C CNN
F 2 "" H 4650 6500 50  0001 C CNN
F 3 "~" H 4650 6500 50  0001 C CNN
	1    4650 6500
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 62B4BBAA
P 4250 6700
F 0 "#FLG0103" H 4250 6775 50  0001 C CNN
F 1 "PWR_FLAG" H 4250 6873 50  0000 C CNN
F 2 "" H 4250 6700 50  0001 C CNN
F 3 "~" H 4250 6700 50  0001 C CNN
	1    4250 6700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4650 6500 4650 6700
$Comp
L Device:Antenna_Chip AE1
U 1 1 62B20BB0
P 1350 4050
AR Path="/60B3AA65/62B20BB0" Ref="AE1"  Part="1" 
AR Path="/62463654/62B20BB0" Ref="AE2"  Part="1" 
F 0 "AE2" H 1270 4089 50  0000 R CNN
F 1 "Antenna_Shield" H 1270 3998 50  0000 R CNN
F 2 "_RF:3216_rf2-4ghz_antenna" H 1350 4150 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/products/datasheet/wireless/An_Ceramiz_2450_3216_L00_0.pdf" H 1350 4150 50  0001 C CNN
F 4 "ANT3216LL00R2400A" H 1350 4050 50  0001 C CNN "Ref."
	1    1350 4050
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 62443EAC
P 4650 6700
F 0 "#FLG01" H 4650 6775 50  0001 C CNN
F 1 "PWR_FLAG" H 4650 6873 50  0000 C CNN
F 2 "" H 4650 6700 50  0001 C CNN
F 3 "~" H 4650 6700 50  0001 C CNN
	1    4650 6700
	-1   0    0    1   
$EndComp
$EndSCHEMATC
