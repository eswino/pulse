EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "pulseWristBand"
Date "2022-03-26"
Rev "v004"
Comp "IKNX - ESWINO Corps"
Comment1 "accelerometer_sheet"
Comment2 "Designer: fmaw"
Comment3 "Reviewer: ace"
Comment4 "Manufacturer : IKNX "
$EndDescr
$Comp
L Sensor_Motion:ADXL343 U10
U 1 1 623FAFD6
P 2150 1650
F 0 "U10" H 2650 2100 50  0000 L CNN
F 1 "ADXL343" H 2650 2000 50  0000 L CNN
F 2 "Package_LGA:LGA-14_3x5mm_P0.8mm_LayoutBorder1x6y" H 2150 1650 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADXL343.pdf" H 2150 1650 50  0001 C CNN
	1    2150 1650
	1    0    0    -1  
$EndComp
Text GLabel 1250 1650 0    50   Input ~ 0
SDA
Text GLabel 1250 1750 0    50   Input ~ 0
SCL
Text GLabel 3100 1650 2    50   Input ~ 0
IT2
Text GLabel 3100 1750 2    50   Input ~ 0
IT3
Text Notes 1800 3150 0    50   ~ 0
ADDR [BUS]__:__0x53\nADDR [BYTE]_:__0xA6\nADDR [READ]_:__0xA7 
$Comp
L power:+3.3V #PWR?
U 1 1 62400B0C
P 4650 1550
AR Path="/62400B0C" Ref="#PWR?"  Part="1" 
AR Path="/60B3AA65/62400B0C" Ref="#PWR?"  Part="1" 
AR Path="/619A2613/62400B0C" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 4650 1400 50  0001 C CNN
F 1 "+3.3V" H 4665 1723 50  0000 C CNN
F 2 "" H 4650 1550 50  0001 C CNN
F 3 "" H 4650 1550 50  0001 C CNN
	1    4650 1550
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62400B14
P 4650 1800
AR Path="/60B3AA65/62400B14" Ref="R?"  Part="1" 
AR Path="/62400B14" Ref="R?"  Part="1" 
AR Path="/619A2613/62400B14" Ref="R13"  Part="1" 
F 0 "R13" H 4700 1975 50  0000 C CNN
F 1 "10k" H 4750 1675 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4580 1800 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 4650 1800 50  0001 C CNN
F 4 "Yageo" H 4650 1800 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-074K7L" H 4650 1800 50  0001 C CNN "Ref."
	1    4650 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 62400B1D
P 4250 1550
AR Path="/60B3AA65/62400B1D" Ref="#PWR?"  Part="1" 
AR Path="/62400B1D" Ref="#PWR?"  Part="1" 
AR Path="/619A2613/62400B1D" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 4250 1300 50  0001 C CNN
F 1 "GNDD" H 4254 1395 50  0000 C CNN
F 2 "" H 4250 1550 50  0001 C CNN
F 3 "" H 4250 1550 50  0001 C CNN
	1    4250 1550
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 62402115
P 4250 1800
AR Path="/60B3AA65/62402115" Ref="R?"  Part="1" 
AR Path="/62402115" Ref="R?"  Part="1" 
AR Path="/619A2613/62402115" Ref="R12"  Part="1" 
F 0 "R12" H 4200 1650 50  0000 C CNN
F 1 "10k" H 4150 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4180 1800 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/447/PYu_RC_Group_51_RoHS_L_10-1664068.pdf" H 4250 1800 50  0001 C CNN
F 4 "Yageo" H 4250 1800 50  0001 C CNN "Manufacturer"
F 5 "RC0402FR-074K7L" H 4250 1800 50  0001 C CNN "Ref."
	1    4250 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	4250 1650 4250 1550
Wire Wire Line
	4650 1550 4650 1650
Wire Wire Line
	4650 1950 4650 2050
Wire Wire Line
	4250 1950 4250 2050
NoConn ~ 1950 1150
NoConn ~ 2350 2150
Wire Wire Line
	1650 1650 1250 1650
Wire Wire Line
	1250 1750 1650 1750
Wire Wire Line
	2650 1650 3100 1650
Wire Wire Line
	3100 1750 2650 1750
$Comp
L power:+3.3V #PWR?
U 1 1 62416133
P 2150 850
AR Path="/62416133" Ref="#PWR?"  Part="1" 
AR Path="/60B3AA65/62416133" Ref="#PWR?"  Part="1" 
AR Path="/619A2613/62416133" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 2150 700 50  0001 C CNN
F 1 "+3.3V" H 2165 1023 50  0000 C CNN
F 2 "" H 2150 850 50  0001 C CNN
F 3 "" H 2150 850 50  0001 C CNN
	1    2150 850 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2150 850  2150 950 
Wire Wire Line
	2150 950  2350 950 
Wire Wire Line
	2350 950  2350 1150
Connection ~ 2150 950 
Wire Wire Line
	2150 950  2150 1150
Text Label 4250 2050 0    50   ~ 0
addr_adxl
Text Label 4650 2050 0    50   ~ 0
cs_adxl
Text Label 1650 1550 2    50   ~ 0
addr_adxl
Text Label 1650 1850 2    50   ~ 0
cs_adxl
$Comp
L power:GNDD #PWR?
U 1 1 62417092
P 2150 2350
AR Path="/60B3AA65/62417092" Ref="#PWR?"  Part="1" 
AR Path="/62417092" Ref="#PWR?"  Part="1" 
AR Path="/619A2613/62417092" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 2150 2100 50  0001 C CNN
F 1 "GNDD" H 2154 2195 50  0000 C CNN
F 2 "" H 2150 2350 50  0001 C CNN
F 3 "" H 2150 2350 50  0001 C CNN
	1    2150 2350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2150 2150 2150 2350
$EndSCHEMATC
